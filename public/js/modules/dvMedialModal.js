/**
 * Manage the Media modal events of the selected field group.
 * 
 * Declare the event handlers for opening and closing the media modal, selecting and unselecting media files for the given fieldgroup.
 * 
 * @since 1.0.0
 * 
 * @param {object} fieldGroup
 * @see https://codex.wordpress.org/Javascript_Reference/wp.media
 * @return void
 */
const DVMediaLibraryModal = fieldGroup => {

    /**
    * 
    */
    let frame;

    /**
    * 
    */
     const key = fieldGroup.key;

    /**
     * The HMTL element containing the preview image via source attribute.
     * 
     * @since 1.0.2
     * 
     * @var object
     */
    const imgContainer = fieldGroup.imgContainer;

    /**
     * The HTML field containing the ID of the selected media file.
     * 
     * @since 1.0.2
     * 
     * @var object
     */
    const fileIDField = fieldGroup.fileIDField;

    /**
     * The HTML trigger element for removing the image from selection.
     * 
     * @since 1.0.2
     * 
     * @var object
     */
    const rmImg = fieldGroup.rmImg

    /**
     * The text that appears at the top of the modal.
     * 
     * @since 1.0.2
     * 
     * @var string
     */
    const modalTitle = fieldGroup.modalTitle ? fieldGroup.modalTitle : 'Select an image.';

    /**
     * The modal file select button text.
     * 
     * @since 1.0.2
     * 
     * @var string
     */
    const modalBtnTxt = fieldGroup.modalBtnTxt ? fieldGroup.modalBtnTxt : 'Use this image.';

    /**
     * Multiselection setting. Defaults to false.
     * 
     * @since 1.0.2
     * 
     * @var boolean
     */
    const multiSelect = fieldGroup.multiSelect ? fieldGroup.multiSelect : false;

    /**
     * 
     */
    let wp_media_post_id = wp.media.model.settings.post.id;

    /**
     * Handle the file selection process.
     * 
     * Set the value of the 
     * 
     * @since 1.0.2
     * 
     * @return void
     */
    const selectFileHandler = () => {
        /** Media file frame state */
        const attachment = frame.state().get('selection').first().toJSON();

        imgContainer.attr('src', attachment.url).css('width', 'auto');
        fileIDField.val(attachment.id);
        rmImg.removeClass('hidden');
        wp.media.model.settings.post.id = wp_media_post_id;

        console.log('selected');
        console.log(wp_media_post_id );
    }

    /**
     * Handle the Modal opening event.
     * 
     * @since 1.0.2
     * 
     * @return void
     */
    const openModalHandler = event => {
        event.preventDefault();
        console.log('opened');
        console.log(key);
        console.log(frame);
        if (frame) {
            frame.uploader.uploader.param('post_id', key);
            frame.open();
            return;
        } else {
            wp.media.model.settings.post.id = key;
        }

        frame = wp.media.frames.file_frame = wp.media({
            title: modalTitle,
            button: {
                text: modalBtnTxt,
            },
            multiple: multiSelect
        });

        frame.on('select', selectFileHandler);
        frame.open();
    }

    /**
     * Remove media file from selection.
     * 
     * Empty the preview container, image source field and hide the remove button.
     * 
     * @since 1.0.2
     * 
     * @param {object} event 
     */
    const rmFileHandler = event => {
        imgContainer.attr('src', '').css('width', 'auto');
        fileIDField.val('');
        event.target.classList.add('hidden');
    }

    fieldGroup.uploadButtons.on('click', openModalHandler);
    rmImg.on('click', rmFileHandler);
    jQuery('a.add_media').on('click', () => wp.media.model.settings.post.id = wp_media_post_id);
};

export default DVMediaLibraryModal