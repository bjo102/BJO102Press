/**
 * @file WooCommerce functions.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Assets\JavaScript
 */

window.addEventListener('load', () => dvWcCart(), false);

/**
 * Set event handlers for showing the hidden shopping cart.
 * 
 * Certain elements are looked for, and if found, event listeners
 * are applied which allow the cart to re-appear on the page.
 * 
 * At this point the shopping cart should have been evaluated,
 * and hidden if empty, with the webpage body having a class to reflect
 * the state of being empty.
 * 
 * @since 1.0.0
 * 
 * @see DV_Utils::toggle_cart()
 * @returns void
 */
function dvWcCart() {
   
   /**
    * Cart data
    * 
    * Localised object from server containing information for HTML elements.
    * 
    * @since 1.0.1
    * 
    * @type {object}
    */
    const data = dvCartData;
    
   /**
    * The webpage body.
    * 
    * @since 1.0.0
    * 
    * @type {array|object}
    */
   const body = document.getElementsByTagName('body')[0];

   /**
    * The WC cart element/container.
    * 
    * @since 1.0.0
    * 
    * @type {array|object}
    */
   const wcCart = document.querySelector(`.${data.cartClass}`);

   /**
    * Possible elements that can reveal the WC cart.
    * 
    * Defines the list of posible elements that could be used to toggle the hidden cart.
    * 
    * @since 1.0.0
    * 
    * @type {array|object}
    */
   const searchEls = [
      ...document.querySelectorAll('.add_to_cart_button'),
      ...document.querySelectorAll('.ajax_add_to_cart'),
      ...document.querySelectorAll('.tribe-tickets__tickets-buy'),
   ];

   /**
    * Existing elements on the page that can reveal the WC cart.
    * 
    * @since 1.0.0
    * 
    * @type {array|object}
    */
   const toggleEls = searchEls.filter(el => el != null);

   /**
    * Show the cart.
    * 
    * This is the event handler for all elements which trigger the cart display.
    * When one of these elements is clicked, the hidden WC Cart is shown.
    * The event listeners from all elements are then removed.
    * 
    * The empty-cart body class is also removed, which is usefull for CSS transitions.
    * 
    * @since 1.0.0
    * 
    * @returns void
    */
   function showCart() {
      wcCart.style.display = 'block';
      setTimeout(() => {
         body.classList.remove(data.bodyClass);
      }, 200);
      toggleEls.forEach(button => {
         button.removeEventListener('click', arguments.callee);
      });
   };

   // If the cart is empty, and you're on a page which has "toggle" elements, 
   if (body.classList.contains(data.bodyClass) && toggleEls.length > 0) {
      toggleEls.forEach(el => {
         el.addEventListener('click', showCart);
      });
   }
};