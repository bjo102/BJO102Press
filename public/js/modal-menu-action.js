/**
 * @file Stop modal links from activating a scroll action
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Assets
 */

/**
 * The modal links array.
 * 
 * @since 1.0.0
 * 
 * @var array
 */
const modalItems = Array.from(document.querySelectorAll( '#header-nav-menu .modal-item'));

if( modalItems.length > 0 ) modalItems.forEach( item => {
  item.querySelector('a').setAttribute('href','javascript: void(0)');
});