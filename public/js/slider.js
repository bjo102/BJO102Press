/**
 * Can also be used with $(document).ready()
 * 
 * https://github.com/woocommerce/FlexSlider/wiki/FlexSlider-Properties 
 */
jQuery(document).ready(function($) {
    // slider settings
    $('.flexslider').flexslider({
        animation: "fade",
        nextText: "",
        prevText: "",
    });

    // let the slider enter the page smoothly
    setTimeout( () => { $('.flexslider').removeClass('not_loaded'); }, 200 ); 
});