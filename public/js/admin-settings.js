import DVMediaLibraryModal from './modules/dvMedialModal.js'

/**
 * Declare BJO102Press admin settings functions.
 * 
 * @since 1.0.2
 */

jQuery(document).ready( $ => {

    /** 
     * The html id attribute of the login logo image field.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    const loginLogoId = dvwpAdminOptions.login_logo_img.id;

    /**
     * @since 1.0.0
     * 
     * @var string
     */
    const loginLogoKey = dvwpAdminOptions.login_logo_img.key;

    /**
     * The html id attributes of the color pickers.
     * 
     * @since 1.0.2
     * 
     * @var string
     */
    const cpIds = [
        dvwpAdminOptions.login_submit_button_colour.id,
        dvwpAdminOptions.login_submit_button_hover_colour.id,
        dvwpAdminOptions.login_submit_font_colour.id,
        dvwpAdminOptions.login_submit_font_hover_colour.id,
        dvwpAdminOptions.login_background_colour.id,
        dvwpAdminOptions.login_accent.id
    ];

    /**
     * @since 1.0.0
     * 
     * @var string
     */
    const adminBarLogoId = dvwpAdminOptions.admin_bar_logo_img.id;

    /**
     * @since 1.0.0
     * 
     * @var string
     */
    const adminBarLogoKey = dvwpAdminOptions.admin_bar_logo_img.key;

    /**
     * Modal Settings for Login logo logo image selection.
     * 
     * @since 1.0.0
     * 
     * @var object
     */
    const loginLogoModal = {
        uploadButtons: $( '.' + loginLogoId + '-upload-trigger' ),
        key: loginLogoKey,
        imgContainer: $( '#' + loginLogoId + '-preview' ),
        fileIDField: $( '#' + loginLogoId ),
        rmImg: $( '.delete-' + loginLogoId ),
        modalTitle: 'Select an image for the login logo.'
    };

    /**
     * Modal Settings for Admin bar logo image selection.
     * 
     * @since 1.0.0
     * 
     * @var object
     */
    const adminBarLogoModal = {
        uploadButtons: $( '.' + adminBarLogoId + '-upload-trigger' ),
        key: adminBarLogoKey,
        imgContainer: $( '#' + adminBarLogoId + '-preview' ),
        fileIDField: $( '#' + adminBarLogoId ),
        rmImg: $( '.delete-' + adminBarLogoId )
    }

    DVMediaLibraryModal(loginLogoModal);
    DVMediaLibraryModal(adminBarLogoModal);
    cpIds.map(id => $(`#${id}`).wpColorPicker());
});