/**
 * Declare functions for DV_Posts ajax.
 *
 * @file   This files defines functions fo the DV_Posts PHP class.
 * @author Byron O'Malley
 * @since  1.0.0
 */

/**
 * The wrapper function for DV_Posts ajax requests.
 * 
 * Archive filter elements and events are defined.
 * 
 * @since 1.0.0
 * 
 * @returns void
 */
jQuery(document).ready($ => {

	/** 
	 * The current url in the browser.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {string}
	*/
	const url = new URL(window.location.href);

	/**
	 * Current state of URL & query vars.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {string}
	 */
	const params = new URLSearchParams(url.search.slice(1));

	/**
	 * The taxonomy term filter HTML elements.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {array|object}
	 */
	const termFilters = $('.terms-list_item');


	/**
	 * The HTML elements.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {array|object}
	 */
	const pageFilters = $('.pag_item');

	/**
	 * The sort-order filter HTML element.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {array|object}
	 */
	const sortFilter = $('.dv-filter-sort');

	/**
	 * The search filter HTML element.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {array|object}
	 */
	const searchFilter = $('.dv-filter-search');

	/**
	 * The archive container.
	 * 
	 * @since 1.0.0
	 * 
	 * @type {object}
	 */
	const archive = $('.dv-posts');

	/**
	 * Default ajax query args.
	 * 
	 * This is the object sent in the ajax request, and is
	 * amended throughout the DV_Posts ajax functions. 
	 * 
	 * @since 1.0.0
	 * 
	 * @type {object}
	 */
	const data = {
		action: 'dvFilterArchive',
		prefix: dvAjaxObj.prefix,
		backupImg: dvAjaxObj.backupImg,
		taxonomies: dvAjaxObj.taxonomies,
		wpQArgs: dvAjaxObj.wpQArgs,
	};

	/**
	 * The search filter submission event action.
	 * 
	 * @since 1.0.0
	 * 
	 * @param {object} e The submission event.
	 * @returns void
	 */
	function searchSubmit(e) {
		let submit = $('.dv-filter-search-input');

		e.preventDefault();
		archive.addClass('transitioning');
		//let input = $this.find('.dv-filter-serarch-input');
		params.delete('dv_page');
		if (submit.val()) {
			params.set('dv_search', submit.val());
		} else {
			params.delete('dv_search');
		}
		data.wpQArgs.s = submit.val();
		data.wpQArgs.paged = 1;

		ajaxCall(data);
	}

	/**
	 * The term filter event action.
	 * 
	 * @since 1.0.0
	 * 
	 * @returns void
	 */
	function filterByTerm() {

		/**
		 * The containing taxonomy filter list.
		 */
		let theList = $(this).parents('.dv-posts-taxonomy-filter');

		/**
		 * The tax queries from WP Query arguements.
		 */
		let taxQs = data.wpQArgs.tax_query ? data.wpQArgs.tax_query : null;

		/**
		 * The current taxonomy relation.
		 */
		let taxRel = 'AND';

		/**
		 * Tax queries to apply to WP Query arguements.
		 */
		let theTaxQs = {};

		/**
		 * The selected taxonomy.
		 */
		let selectedTax = theList.data('taxonomy');

		/**
		 * The query varible key.
		 */
		let taxQVarKey = `${data.prefix}_${selectedTax}`;

		archive.addClass('transitioning');

		if (taxQs.relation) {
			taxRel = taxQs.relation;
			delete taxQs.relation;
		}

		Object.values(taxQs).forEach(t => {
			let tax = t.taxonomy;
			let terms = t.terms;
			theTaxQs[tax] = new TaxQuery(tax, terms);
		});


		// is the clicked button active?
		if (!$(this).hasClass('active')) {

			params.set(taxQVarKey, $(this).data('slug'));
			theList.find('.terms-list_item').removeClass('active');
			$(this).addClass('active');

			theTaxQs[selectedTax] = new TaxQuery(selectedTax, $(this).data('slug'));
		} else {
			params.delete(taxQVarKey);
			$(this).removeClass('active');
			delete theTaxQs[selectedTax];
		}

		funalTax = {
			relation: taxRel
		}

		var arr = Object.values(theTaxQs);

		for (const [index, value] of arr.entries()) {
			funalTax[index] = value;
		}

		params.delete('dv_page');
		data.wpQArgs.paged = 1;
		data.wpQArgs.tax_query = funalTax;
		ajaxCall(data);
	};

	/**
	 * Template function for tax query.
	 * 
	 * @since 1.0.1
	 * 
	 * @param {string} taxonomy 
	 * @param {string} slug 
	 */
	function TaxQuery(taxonomy, slug) {
		this.taxonomy = taxonomy;
		this.field = 'slug';
		this.terms = slug;
		this.operator = 'IN';
	}

	/**
	 * The pagination event action.
	 * 
	 * @since 1.0.0
	 * 
	 * @returns void
	 */
	function paginate() {
		archive.addClass('transitioning');

		params.set('dv_page', $(this).data('page'));
		$('.pag_item').removeClass('active');
		$(this).addClass('active');
		data.wpQArgs.paged = $(this).data('page');

		ajaxCall(data);
	};

	/**
	 * The sort-order event action.
	 * 
	 * @since 1.0.0
	 * 
	 * @returns void
	 */
	function sort() {
		archive.addClass('transitioning');

		params.set('dv_sort', $(this).val());
		params.delete('dv_page');
		data.sort = $(this).val();
		data.wpQArgs.paged = 1;
		//if( $( '.cat-list_item.active' ) ) data.eventCategory = $( '.cat-list_item.active' ).first().data( 'slug' );

		ajaxCall(data);
	};

	/**
	 * Call the ajax request.
	 * 
	 * - Update the URL.
	 * - Send ajax POST request with data.
	 * - Re-render the page.
	 * - Re-apply event listeners.
	 * 
	 * @since 1.0.0
	 * 
	 * @param {object} data The data object to be given to the server.
	 * @returns void
	 */
	function ajaxCall(data) {
		urlUpdate();
		const args = {
			type: 'POST',
			url: dvAjaxObj.ajaxUrl,
			dataType: 'html',
			data: data,
			success: response => {
				archive.html(response);
				$('.pag_item').on('click', paginate);
				archive.removeClass('transitioning');
				scrollUpOnPagination();
				linkEffects();
			},
			error: (xhr, status, error) => {
				const errorMessage = `Error - ${xhr.status}: ${xhr.statusText}`;
				archive.html(errorMessage);
			}
		}
		// Send ajax request
		$.ajax(args);
	};

	/**
	 * Update the current URL.
	 * 
	 * Use query variables to reform the URL and update the browser history.
	 * 
	 * @since 1.0.0
	 * 
	 * @returns void
	 */
	function urlUpdate() {
		let newUrl = url.origin + url.pathname
		if (params.toString()) newUrl += `?${params.toString()}`;

		let stateObj = null;
		history.pushState(stateObj, '', newUrl);
	};

	/**
	 * Scroll back to the top of the archvie when using pagination.
	 * 
	 * @since 1.0.0
	 * 
	 * @returns void
	 */
	function scrollUpOnPagination() {
		const scrollEl = document.querySelector('.dv-posts-container');
		const pos = scrollEl.getBoundingClientRect().top + window.scrollY;
		const pageBtns = document.querySelectorAll('.pag_item');
		pageBtns.forEach(btn => btn.addEventListener('click', function () {
			//scrollEl.scrollIntoView({ behavior: "smooth", inline: "nearest" });
			window.scroll({
				top: pos - 100,
				behavior: 'smooth'
			});
		}));

		//const link = e.target.getAttribute(`data-${anchorAttr}-link`)
		//const anchor = document.querySelector(`*[data-dvwp-content-anchor="${link}"]`);
		//const anchorPos = anchor.getBoundingClientRect().top + window.scrollY;
	};

	/**
	 * Apply effects for links in each post.
	 * 
	 * @since 1.0.0
	 * 
	 * @returns void
	 */
	function linkEffects() {
		let evLinks = document.querySelectorAll('.event_link');
		evLinks.forEach(link => {
			link.addEventListener('mouseover', themouseover);
			link.addEventListener('mouseout', themouseout);
		});

		function themouseover() {
			this.closest('.event_post').classList.add('link_hover');
		}
		function themouseout() {
			this.closest('.event_post').classList.remove('link_hover');
		}
	};

	data.sort = sortFilter.val();

	scrollUpOnPagination();
	linkEffects();

	termFilters.on('click', filterByTerm);
	pageFilters.on('click', paginate);
	searchFilter.submit(e => searchSubmit(e));
	sortFilter.on('change', sort);
});
