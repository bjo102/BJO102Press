<?php

/**
 * Plugin Name: BJO102Press WP
 * Plugin URI: https://bjo102.dev/
 * Description: Additional utilities & branding options for your WordPress website.
 * Version: 1
 * Author: bjo102
 * Author URI: https://bjo102.dev/
 * Text Domain: bjo102press
 *
 * @version  1.0.3
 * @author Byron O'Malley
 * @package bjopress
 * 
 * @todo Add meta fields to posts for whether to add dv anchor links to the content for navigation.
 */

if (! defined('ABSPATH')) exit;

/**
 * The current BJO102Press plugin version.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
! defined('DVWP_PLUGIN_VERSION') | define('DVWP_PLUGIN_VERSION', '1.0.3');

/**
 * The BJO102Press plugin PATH.
 * 
 * This contains the trailing forward slash ("/").
 * 
 * @since 1.0.0
 * 
 * @var string
 */
! defined('DVWP_PLUGIN_PATH') | define('DVWP_PLUGIN_PATH', plugin_dir_path( __FILE__ ));

/**
 * The BJO102Press plugin views PATH.
 * 
 * This contains the trailing forward slash ("/").
 * 
 * @since 1.0.3
 * 
 * @var string
 */
! defined('DVWP_VIEWS_PATH') | define('DVWP_VIEWS_PATH', plugin_dir_path( __FILE__ ) . "src/Views/");

/**
 * The BJO102Press plugin URL.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
! defined('DVWP_PLUGIN_URL') | define('DVWP_PLUGIN_URL', plugin_dir_url( __FILE__ ));

/**
 * The BJO102Press plugin CSS URL.
 * 
 * This contains the trailing forward slash ("/").
 * 
 * @since 1.0.3
 * 
 * @var string
 */
! defined('DVWP_CSS_URL') | define('DVWP_CSS_URL', plugin_dir_url( __FILE__ ) . 'public/css/');

/**
 * The BJO102Press plugin JavaScript URL.
 * 
 * This contains the trailing forward slash ("/").
 * 
 * @since 1.0.3
 * 
 * @var string
 */
! defined('DVWP_JS_URL') | define('DVWP_JS_URL', plugin_dir_url( __FILE__ ) . 'public/js/');

/**
 * The BJO102Press website URL.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
! defined('DVWP_SITE_URL') | define('DVWP_SITE_URL', 'https://bjo102.dev/');

/**
 * The BJO102Press logo URL.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
! defined('DVWP_LOGO_URL') | define('DVWP_LOGO_URL', plugin_dir_url(__FILE__) . '/public/img/bjofav.png');

/**
 * The site admin user role name.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
! defined('DVWP_CLIENT_ADMIN_ROLE') | define('DVWP_CLIENT_ADMIN_ROLE', 'dv_site_admin');

/**
 * The HTML custom data attribute key for BJO102Press custom anchors.
 * 
 * @since 1.0.1
 * 
 * @var string
 */
! defined('DVWP_CUSTOM_HTML_ANCHOR') | define('DVWP_CUSTOM_HTML_ANCHOR', 'dvwp-content-anchor');

/**
 * 
 * @since 1.0.3
 * 
 * @var string
 */
! defined('DVWP_COMPOSE_VENDOR_PATH') | define('DVWP_COMPOSE_VENDOR_PATH', __DIR__ . '/vendor/autoload.php');


require DVWP_COMPOSE_VENDOR_PATH;
use DVWP\Classes;
$dvwp = new DVWP\Classes\Main();