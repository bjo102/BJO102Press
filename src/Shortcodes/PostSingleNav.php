<?php

/**
 * BJO102Press Shortcode Functions: PostSingleNav.
 * 
 * @since 1.0.2
 * 
 * @package BJO102Press\Shortcodes
 */

namespace DVWP\Shortcodes;
use DVWP\Classes\Utils;

if (!defined('ABSPATH')) exit;

if (!function_exists('DVWP\Shortcodes\PostSingleNav')) :
    /**
     * The HTML template for the DV Post Single Navigation shortcode.
     * 
     * This uses DOM nodes pulled from the current post to create navigation
     * links and scroll-to events, with custom data attibutes and JavaScript, not ids.
     * 
     * Attributes include the following:
     * 
     * - heading_lvl: The level of the navigation heading, a number between 2 and 6.
     * - scroll_threshold: The ammount of scrolling needed to alter the navigations class, this is usefull if the navigation is in a sticky container.
     * - anchor_scroll_offset: The overstep taken when scrolling to anchors, so the headings aren't hidden behind a navbar.
     * 
     * @since 1.0.0
     * 
     * @access public
     * @param array $atts Shortcode attributes.
     * @return string The shortcode template as HTML.
     */
    function PostSingleNav($atts)
    {
        $atts = shortcode_atts(
            [
                'heading_lvl'           => 3,
                'scroll_threshold'      => 500,
                'anchor_scroll_offset'  => 100
            ],
            $atts,
            'dv_posts'
        );

        $headingLvl             = (int) intval($atts['heading_lvl']);
        $scrollThreshold        = (int) intval($atts['scroll_threshold']);
        $anchorScrollOffset     = (int) intval($atts['anchor_scroll_offset']);

        if ($headingLvl > 6) $headingLvl = 6;
        if ($headingLvl < 2) $headingLvl = 2;

        $html = (string) '';
        $post_id = get_the_ID();
        $content = get_the_content(null, false, $post_id);

        $domData = Utils::get_prepared_dom_achors($content, '//h2|//h3');
        $nodes = $domData->nodes;

        if (!empty($nodes)) :
            $prevNodeTagName = '';
            $html .= '
            <nav class="dv-post-navigation">
            <h' . strval($headingLvl) . '>CONTENT</h' . strval($headingLvl) . '>
            <ul>';
            foreach ($nodes as $nodeObj) :
                $label = $nodeObj->node->textContent;
                $tagName = $nodeObj->node->tagName;
                $anchor = $nodeObj->anchor;
                $listItem = '
                <li>
                    <button class="dv-post-navigation-link" data-' . DVWP_CUSTOM_HTML_ANCHOR . '-link="' . esc_attr($anchor) . '">' . esc_html($label) . '</button>
                </li>';

                if (preg_match('/(h3)|(h4)|(h5)|(h6)/', $tagName) && $prevNodeTagName == 'h2' && $tagName != 'h2') {
                    $listItem = '<ul>' . $listItem;
                }
                if ($tagName == 'h2' && preg_match('/(h3)|(h4)|(h5)|(h6)/', $prevNodeTagName)) {
                    $listItem = '</ul>' . $listItem;
                }

                $html .= $listItem;
                $prevNodeTagName = $tagName;
            endforeach;
            $html .= '
            </ul>
            </nav>';
        endif;

        $data = [
            'anchorAttr' => DVWP_CUSTOM_HTML_ANCHOR,
            'scrollThreshold' => $scrollThreshold,
            'anchorScrollOffset' => $anchorScrollOffset
        ];

        wp_localize_script('dv-post-single-navigation', 'dvPSNData', $data);
        wp_enqueue_script('dv-post-single-navigation');

        return $html;
    };
endif;
