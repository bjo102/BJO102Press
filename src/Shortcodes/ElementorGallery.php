<?php

/**
 * BJO102Press Shortcode Functions: ElementorGallery.
 * 
 * @since 1.0.2
 * 
 * @package BJO102Press\Shortcodes
 */

namespace DVWP\Shortcodes;
use DVWP\Classes\Shortcodes;
use DVWP\Classes\ElementorGallery;

if (!defined('ABSPATH')) exit;

if (!function_exists('DVWP\Shortcodes\ElementorGallery')) :
    /**
     * The HTML template for the DV Gallery shortcode.
     * 
     * @since 1.0.3
     * 
     * @param array $atts Shortcode attributes.
     * @return string The shortcode template as HTML.
     */
    function ElementorGallery($atts)
    {
        $atts = shortcode_atts(
            array(
                'featured' => false,
                'product_gallery' => false,
                'ids' => '',
                'gallery_id' => null
            ),
            $atts,
            'dv_elementor_gallery'
        );

        $args = [];

        if (isset($atts['featured'])) {
            $args['featured'] = $atts['featured'];
        }
        if (isset($atts['product_gallery'])) {
            $args['product_gallery'] = $atts['product_gallery'];
        }
        if (isset($atts['ids'])) {
            $args['ids'] = Shortcodes::breakAttr($atts['ids']);
        }
        if (null != $atts['gallery_id']) {
            $args['gallery_id'] = $atts['gallery_id'];
        }

        $gallery = new ElementorGallery($args);
        $html = $gallery->get_html();

        return $html;
    };
endif;