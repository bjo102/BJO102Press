<?php

/**
 * BJO102Press Shortcode Functions: Posts.
 * 
 * @since 1.0.3
 * 
 * @package BJO102Press\Shortcodes
 */

namespace DVWP\Shortcodes;
use DVWP\Classes\Posts;
use DVWP\Classes\Shortcodes;

if (! defined('ABSPATH')) exit;

if (! function_exists('DVWP\Shortcodes\Posts')) :
    /**
     * The HTML template for the DV Posts shortcode.
     * 
     * This will instantiate a DV_Posts object, which in turn enqueues scripts for Ajax requests.
     *      
     *     [dv_posts post_type="post" taxonomies="category, post_tag" tax_query_relation="AND"]
     * 
     * Atttributes include standard WP Query arguments.
     * 
     * - post_type: The post type to query. Only one permitted.
     * - posts_per_page
     * - order_by: The value to order the posts by.
     * - order: The default order of posts.
     * - backup_id: The media file ID which appears for posts with no featured image.
     * - taxonomies: The archive taxonomies used to produce separate taxonomy term filters. These should be valid slugs for taxonomies and seperated by commas.
     * - tax_query_relation: The relationship between terms selected in the different taxonomy lists: "AND" (narrow) or "OR" (wide) selection. 
     * 
     * @since 1.0.3
     * 
     * @param array $atts Shortcode attributes.
     * @return string The shortcode template as HTML.
     */
    function Posts($atts)
    {
        $html = '';

        // Default shortcode values.
        $atts = shortcode_atts(
            array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 9,
                'orderby' => 'date',
                'order' => 'DESC',
                'backup_img' => null,
                'taxonomies' => '',
                'tax_query_relation' => 'OR',
            ),
            $atts,
            'dv_posts'
        );

        // Post type must be valid.
        if (!post_type_exists($atts['post_type'])) {
            return '<p>No valid post types were given.</p>';
        }

        // Declare arguements.
        $wpQArgs = (array) [
            'post_type' => sanitize_text_field($atts['post_type']),
            'post_status' => sanitize_text_field($atts['post_status']),
            'posts_per_page' => filter_var($atts['posts_per_page'], FILTER_SANITIZE_NUMBER_INT),
            'orderby' => sanitize_text_field($atts['orderby']),
            'order' => sanitize_text_field($atts['order']),
        ];
        $backupImg = (int) $atts['backup_img'];
        $taxonomies = (array) Shortcodes::breakAttr($atts['taxonomies']);

        // Prepare taxonomy relation.
        if(!empty($taxonomies)) {
            $wpQArgs['tax_query'] = [];
            if (in_array($atts['tax_query_relation'], ['AND', 'OR'])) {
                $wpQArgs['tax_query']['relation'] = sanitize_text_field($atts['tax_query_relation']);
            }
        }

        // Instantiate archive
        $DVPosts = new Posts([
            'wp_query_args' => $wpQArgs,
            'taxonomies' => $taxonomies,
            'use_qvars' => true,
            'backup_img' => $backupImg,
        ]);

        $html .= '<section class="dv-posts-container">';
        $html .= '<div class="dv-posts-filters-container">';
        $html .= $DVPosts->getSearchForm();
        $html .= $DVPosts->getSort();
        $html .= '</div>';
        $html .= '<div class="dv-posts-filters-container">' . $DVPosts->getTaxonomyFilters() . '</div>';
        $html .= $DVPosts->getArchiveContainer();
        $html .= '</section>';

        return $html;
    };
endif;