<?php
/**
 * Declare CSS rules for Login form.
 * 
 * If no image is selected for the login logo, the BJO102Press logo is used instead.
 * 
 * @since 1.0.2
 * 
 * @package BJO102Press\Views
 */

$loginLogo = $this->defaultLogoFile;

if($this->adminSettingsEnabled) {
    // CSS rules to be applied.
    $bodyRules = '';
    $subBtnRules = '';
    $subBtnHvRules = '';
    $inputRules = '';
    $inputHvRules = '';
    $inputFcRules = '';
    $linksRules = '';
    $linksHvRules = '';

    // Options
    $logoOption     = get_option($this->options['login_logo_img']);
    $subColor       = get_option($this->options['login_submit_button_colour']);
    $subHColor      = get_option($this->options['login_submit_button_hover_colour']);
    $subFontColor   = get_option($this->options['login_submit_font_colour']);
    $subFontHColor  = get_option($this->options['login_submit_font_hover_colour']);
    $bkgrndColor    = get_option($this->options['login_background_colour']);
    $accent         = get_option($this->options['login_accent']);

    // apply styles
    if ($subColor) {
        $subBtnRules .= '
        background-color: ' . $subColor . ';
        border-color: ' . $subColor . ';';
    }

    if ($subHColor) {
        $subBtnHvRules .= '
        background-color: ' . $subHColor . ';
        border-color: ' . $subHColor . ';';
    }

    if ($subFontColor) {
        $subBtnRules .= '
        color: ' . $subFontColor. ';';
    }

    if ($subFontHColor) {
        $subBtnHvRules .= '
        color: ' . $subFontHColor. ';';
    }

    if ($bkgrndColor) {
        $bodyRules .= '
        background-color: ' . $bkgrndColor . ';';
    }

    if ($accent) {
        $inputFcRules .= '
        border-color: ' . $accent . ';
        box-shadow: 0 0 0 1px ' . $accent . ';';

        $linksHvRules .= '
        color: ' . $accent . ';';
    }

    if($logoOption) {
        $loginLogo = wp_get_attachment_url($logoOption);
    }
}

?>
<style type="text/css">
    body.login div#login h1 a {
        width: 100%;
        padding-bottom: 30px; 
        background-image: url(<?php echo esc_html($loginLogo); ?>);
        background-size: contain;
    }
    <?php if ($this->adminSettingsEnabled) : ?>
        body.login {
            <?php echo esc_html($bodyRules); ?>
        }
        #login form {
            padding: 0;
            border: none;
            background: transparent;
        }
        #login p#nav,
        #login p#backtoblog {
            padding: 0;
        }

        body.login input, body.login select, body.login textarea {
            transition: all .3s;
        }

        body.login input:focus, body.login select:focus, body.login textarea:focus {
            <?php echo $inputFcRules; ?>
        }
        a:hover,
        .privacy-policy-link,
        .dashicons.dashicons-visibility {
            <?php echo $linksHvRules; ?>
        }
        
        .login p.message {
            border-left: none;
        }
        .login .submit input {
            width: 100%;
            font-weight: 700;
            text-transform: uppercase;
            <?php echo $subBtnRules; ?>
            transition: all .3s;
        }
        .login .submit input:hover {
            <?php echo $subBtnHvRules; ?>
        }
    <?php endif; ?>
</style>
