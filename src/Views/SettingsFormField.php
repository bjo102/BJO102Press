<?php

/**
 * BJO102Press Views: Settings Form Field.
 * 
 * @since 1.0.3
 * 
 * @package BJO102Press\Views
 */

?>

<div class="dvwp-field-container">
    <label for="<?php echo esc_attr($id); ?>"><?php echo esc_html($label); ?></label>
    <?php echo $before; ?>
    <input type="<?php echo esc_attr($type); ?>" name="<?php echo esc_attr($name); ?>" id="<?php echo esc_attr($id); ?>" value="<?php echo esc_attr($val); ?>" />
    <?php echo $after; ?>
</div>