<?php

/**
 * Render the DVWP admin settings page.
 * 
 * @since 1.0.2
 * 
 * @package BJO102Press\Views
 */

use DVWP\Classes\Admin;

$dvAdminOpts = $this->options;
$nonceAction        = 'dvwp_admin_main_settings_nonce_action';
$nonceName          = 'dvwp_admin_main_settings_nonce';

if (
    $_POST
    && (! isset($_POST[$nonceName]) || ! wp_verify_nonce($_POST[$nonceName], $nonceAction)) 
) {
   print 'Sorry, your nonce did not verify.';
   exit;
} else if(
    $_POST
    && (isset($_POST[$nonceName]) && wp_verify_nonce($_POST[$nonceName], $nonceAction)) 
) {

    if (isset($_POST[$dvAdminOpts['admin_styles_enabled']])) {
        update_option($dvAdminOpts['admin_styles_enabled'], true);
    } else {
        update_option($dvAdminOpts['admin_styles_enabled'], false);
    }

    if (isset($_POST[$dvAdminOpts['login_logo_img']])) {
        update_option($dvAdminOpts['login_logo_img'], sanitize_text_field($_POST[$dvAdminOpts['login_logo_img']]));
    }

    if (isset($_POST[$dvAdminOpts['login_logo_link']])) {
        update_option($dvAdminOpts['login_logo_link'], sanitize_text_field($_POST[$dvAdminOpts['login_logo_link']]));
    }

    if (isset($_POST[$dvAdminOpts['login_submit_button_colour']])) {
        update_option($dvAdminOpts['login_submit_button_colour'], sanitize_text_field($_POST[$dvAdminOpts['login_submit_button_colour']]));
    }

    if (isset($_POST[$dvAdminOpts['login_submit_button_hover_colour']])) {
        update_option($dvAdminOpts['login_submit_button_hover_colour'], sanitize_text_field($_POST[$dvAdminOpts['login_submit_button_hover_colour']]));
    }

    if (isset($_POST[$dvAdminOpts['login_submit_font_colour']])) {
        update_option($dvAdminOpts['login_submit_font_colour'], sanitize_text_field($_POST[$dvAdminOpts['login_submit_font_colour']]));
    }

    if (isset($_POST[$dvAdminOpts['login_submit_font_hover_colour']])) {
        update_option($dvAdminOpts['login_submit_font_hover_colour'], sanitize_text_field($_POST[$dvAdminOpts['login_submit_font_hover_colour']]));
    }

    if (isset($_POST[$dvAdminOpts['login_background_colour']])) {
        update_option($dvAdminOpts['login_background_colour'], sanitize_text_field($_POST[$dvAdminOpts['login_background_colour']]));
    }

    if (isset($_POST[$dvAdminOpts['login_accent']])) {
        update_option($dvAdminOpts['login_accent'], sanitize_text_field($_POST[$dvAdminOpts['login_accent']]));
    }

    if (isset($_POST[$dvAdminOpts['login_message']])) {
        update_option($dvAdminOpts['login_message'], sanitize_text_field($_POST[$dvAdminOpts['login_message']]));
    }

    if (isset($_POST[$dvAdminOpts['admin_bar_logo_img']])) {
        update_option($dvAdminOpts['admin_bar_logo_img'], sanitize_text_field($_POST[$dvAdminOpts['admin_bar_logo_img']]));
    }

    if (isset($_POST[$dvAdminOpts['admin_bar_logo_link']])) {
        update_option($dvAdminOpts['admin_bar_logo_link'], sanitize_text_field($_POST[$dvAdminOpts['admin_bar_logo_link']]));
    }

    if (isset($_POST[$dvAdminOpts['admin_bar_logo_alt']])) {
        update_option($dvAdminOpts['admin_bar_logo_alt'], sanitize_text_field($_POST[$dvAdminOpts['admin_bar_logo_alt']]));
    }

    do_action('dvwp_main_settings_page_submission');

}

$optData = $this->getOptionsData();

$adminStylesEnabled = $optData['admin_styles_enabled'];
$loginLogoImg       = $optData['login_logo_img'];
$loginLogoLink      = $optData['login_logo_link'];
$loginSubColor      = $optData['login_submit_button_colour'];
$loginSubHColor     = $optData['login_submit_button_hover_colour'];
$loginSubFontColor  = $optData['login_submit_font_colour'];
$loginSubFontHColor = $optData['login_submit_font_hover_colour'];
$loginBkgdColour    = $optData['login_background_colour'];
$loginAccent        = $optData['login_accent'];
$loginMessage       = $optData['login_message'];

$adminBarLogoImg    = $optData['admin_bar_logo_img'];
$adminBarLogoLink   = $optData['admin_bar_logo_link'];
$adminBarLogoAlt    = $optData['admin_bar_logo_alt'];

wp_enqueue_media();
wp_enqueue_script('wp-color-picker');
wp_enqueue_style('wp-color-picker');

?>

<div id="dvwp-admin-branding-page">
<h1>DVWP Settings</h1>
<form id="dvwp-admin-branding-form" method='post'>
    <section id="dvwp-branding-section">

        <fieldset id="dvwp-login-logo-fieldset" class="dvwp-fieldset">
            <h2>Login Form</h2>
            <p>These settings affect the login page and modals.</p>

            <div class="dvwp-field-container">
                <input type="checkbox" name="<?php echo esc_attr($adminStylesEnabled['key']); ?>" id="<?php echo esc_attr($adminStylesEnabled['id']); ?>" value="enable-admin-styles"
                <?php
                    if ($adminStylesEnabled['value'] == true) {
                        echo " checked";
                    }
                ?>/>
                <label for="<?php echo esc_attr($adminStylesEnabled['id']); ?>">Enable Admin Styles?</label>
            </div>

            <div id="login-logo-image" class="dvwp-field-container">
                <label for="<?php echo esc_attr($loginLogoImg['id']); ?>">Login Form Logo Image:</label>
                <p>This is the image that appears at the top of the login form.</p>
                <div class='cd-admin-image-preview-wrapper'>
                    <img id="<?php echo esc_attr($loginLogoImg['id']); ?>-preview" class="cd-admin-settings-preview-image <?php echo $loginLogoImg['id']; ?>-upload-trigger" src="<?php if($loginLogoImg['value']) echo wp_get_attachment_url($loginLogoImg['value']); ?>"/>
                </div>
                <input type="button" class="button <?php echo $loginLogoImg['id']; ?>-upload-trigger" value="<?php echo _e( 'Select image' ); ?>"/>
                <input type='hidden' name="<?php echo $loginLogoImg['key']; ?>" id="<?php echo $loginLogoImg['id']; ?>" value="<?php echo $loginLogoImg['value']; ?>"/>
                <input type="button" class="button delete-<?php echo $loginLogoImg['id']; ?> <?php if (! $loginLogoImg['value']) { echo 'hidden'; } ?>" value="<?php echo _e('Remove this image'); ?>"/>
            </div>
            
            <?php Admin::settingsFormField('Login Form Logo URL', $loginLogoLink['id'], $loginLogoLink['key'], $loginLogoLink['value'], 'text', '<p>Add a link for when the user clicks the logo. This link is opened in a new tab.</p>'); ?>

            <div class="dvwp-field-container">
                <label for="<?php echo $loginMessage['id']; ?>">Login Form Message:</label><br>
                <?php
                $wpeArgs = [
                    'wpautop' => true, // Replace double line breaks with paragraph elements.
                    'teeny' => false, // Whether to output the minimal editor config.
                    'tinymce' => true,
                    'media_buttons' => false,
                ];
                wp_editor( $loginMessage['value'], $loginMessage['key'], $wpeArgs ); ?>
            </div>
            
            <div id="dvwp-login-form-colors">
                <h3>Login Form Colors</h3>
                <?php
                Admin::settingsFormField('Submit Button Colour', $loginSubColor['id'], $loginSubColor['key'], $loginSubColor['value']);
                Admin::settingsFormField('Submit Button Colour (Mouse-Hover)', $loginSubHColor['id'], $loginSubHColor['key'], $loginSubHColor['value']);
                Admin::settingsFormField('Submit Font Colour', $loginSubFontColor['id'], $loginSubFontColor['key'], $loginSubFontColor['value']);
                Admin::settingsFormField('Submit Font Colour (Mouse-Hover)', $loginSubFontHColor['id'], $loginSubFontHColor['key'], $loginSubFontHColor['value']);
                Admin::settingsFormField('Background Colour', $loginBkgdColour['id'], $loginBkgdColour['key'], $loginBkgdColour['value']);
                Admin::settingsFormField('Form Accent', $loginAccent['id'], $loginAccent['key'], $loginAccent['value']);
                ?>
            </div>
        </fieldset>

        <fieldset id="dvwp-adminbar-logo-fieldset" class="dvwp-fieldset">
            <h2>Admin Bar Logo</h2>

            <div id="adminbar-logo-image" class="dvwp-field-container">
                <label for="<?php echo $adminBarLogoImg['id']; ?>">Image:</label>
                <p class="cd-wpb-instructions">
                    This image will appear next to the name of your site on the top-left side of the admin bar.
                    The recommended width for this image is 50 pixels.
                </p>
                <div class="cd-admin-image-preview-wrapper">
                    <img id="<?php echo $adminBarLogoImg['id']; ?>-preview" class="cd-admin-settings-preview-image <?php echo $adminBarLogoImg['id']; ?>-upload-trigger" src="<?php echo wp_get_attachment_url($adminBarLogoImg['value']); ?>"/>
                </div>
                <input type="button" class="button <?php echo $adminBarLogoImg['id']; ?>-upload-trigger" value="<?php _e( 'Select image' ); ?>"/>
                <input type="hidden" name="<?php echo $adminBarLogoImg['key']; ?>" id="<?php echo $adminBarLogoImg['id']; ?>" value="<?php echo $adminBarLogoImg['value']; ?>"/>
                <input type="button" class="button delete-<?php echo $adminBarLogoImg['id']; ?> <?php if (! $adminBarLogoImg['value']) { echo 'hidden'; } ?>"value="<?php echo _e('Remove this image'); ?>"/>
            </div>
            
            <?php
            Admin::settingsFormField('Admin Bar Logo Link', $adminBarLogoLink['id'], $adminBarLogoLink['key'], $adminBarLogoLink['value']);
            Admin::settingsFormField('Admin Bar Logo Alt Text', $adminBarLogoAlt['id'], $adminBarLogoAlt['key'], $adminBarLogoAlt['value']);
            ?>
        
        </fieldset>

        <?php do_action('dvwp_main_settings_page_after'); ?>

    </section>

    <div id="dvwp-admin-branding-form-submit-container">
        <input type="submit" name="submit_image_selector" value="Save" class="button-primary"/>
        <?php wp_nonce_field( $nonceAction, $nonceName); ?>
    </div>
</form>
</div>