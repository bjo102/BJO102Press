<?php
/**
 * WordPress User Role - Site Admin.
 * 
 * A user role meant to resemble something between an editor and an administrator.
 *
 * @since 1.0.0
 * 
 * @package BJO102Press\User_Roles
 */

namespace DVWP\UserRoles;

if (!defined('ABSPATH')) exit;

if(! function_exists('DVWP\UserRoles\InitClientRole')) :
    /**
     * Initiate the Client Administratior user role.
     * 
     * This is meant to be an editor role, with some administrator privileges.
     * 
     * @since 1.0.3
     */
    function InitClientRole()
    {
        $capabilities = [
            // editor capabilities
            'delete_others_pages' => true,
            'delete_others_posts' => true,
            'delete_pages' => true,
            'delete_posts' => true,
            'delete_private_pages' => true,
            'delete_private_posts' => true,
            'delete_published_pages' => true,
            'delete_published_posts' => true,
            'edit_others_pages' => true,
            'edit_others_posts' => true,
            'edit_pages' => true,
            'edit_posts' => true,
            'edit_private_pages' => true,
            'edit_private_posts' => true,
            'edit_published_pages' => true,
            'edit_published_posts' => true,
            'manage_categories' => true,
            'manage_links' => true,
            'moderate_comments' => true,
            'publish_pages' => true,
            'publish_posts' => true,
            'read' => true,
            'read_private_pages' => true,
            'read_private_posts' => true,
            'unfiltered_html' => true,
            'upload_files' => true,

            // woocommerce
            'manage_woocommerce' => true,
            'view_woocommerce_reports' => true,
            // products
            'edit_product' => true,
            'read_product' => true,
            'delete_product' => true,
            'edit_products' => true,
            'edit_others_products' => true,
            'publish_products' => true,
            'read_private_products' => true,
            'delete_products' => true,
            'delete_private_products' => true,
            'delete_published_products' => true,
            'delete_others_products' => true,
            'edit_private_products' => true,
            'edit_published_products' => true,
            // shop orders
            'edit_shop_order' => true,
            'read_shop_order' => true,
            'delete_shop_order' => true,
            'edit_shop_orders' => true,
            'edit_others_shop_orders' => true,
            'publish_shop_orders' => true,
            'read_private_shop_orders' => true,
            'delete_shop_orders' => true,
            'delete_private_shop_orders' => true,
            'delete_published_shop_orders' => true,
            'delete_others_shop_orders' => true,
            'edit_private_shop_orders' => true,
            'edit_published_shop_orders' => true,
            // coupons
            'edit_shop_coupon' => true,
            'read_shop_coupon' => true,
            'delete_shop_coupon' => true,
            'edit_shop_coupons' => true,
            'edit_others_shop_coupons' => true,
            'publish_shop_coupons' => true,
            'read_private_shop_coupons' => true,
            'delete_shop_coupons' => true,
            'delete_private_shop_coupons' => true,
            'delete_published_shop_coupons' => true,
            'delete_others_shop_coupons' => true,
            'edit_private_shop_coupons' => true,
            'edit_published_shop_coupons' => true,
            // Terms.
            'manage_product_terms' => true,
            'edit_product_terms' => true,
            'delete_product_terms' => true,
            'assign_product_terms' => true,
            'manage_shop_order_terms' => true,
            'edit_shop_order_terms' => true,
            'delete_shop_order_terms' => true,
            'assign_shop_order_terms' => true,
            'manage_shop_coupon_terms' => true,
            'edit_shop_coupon_terms' => true,
            'delete_shop_coupon_terms' => true,
            'assign_shop_coupon_terms' => true,

            // gravity forms
            'gravityforms_view_entries' => true,
            'gravityforms_edit_entries' => true,
            'gravityforms_delete_entries' => true,
            'gravityforms_view_entry_notes' => true,
            'gravityforms_edit_entry_notes' => true,
            'gravityforms_export_entries' => true,

            // the event calendar
            'edit_tribe_event' => true,
            'read_tribe_event' => true,
            'delete_tribe_event' => true,
            'delete_tribe_events' => true,
            'edit_tribe_events' => true,
            'edit_others_tribe_events' => true,
            'delete_others_tribe_events' => true,
            'publish_tribe_events' => true,
            'edit_published_tribe_events' => true,
            'delete_published_tribe_events' => true,
            'delete_private_tribe_events' => true,
            'edit_private_tribe_events' => true,
            'read_private_tribe_events' => true,
            // venues
            'edit_tribe_venue' => true,
            'read_tribe_venue' => true,
            'delete_tribe_venue' => true,
            'delete_tribe_venues' => true,
            'edit_tribe_venues' => true,
            'edit_others_tribe_venues' => true,
            'delete_others_tribe_venues' => true,
            'publish_tribe_venues' => true,
            'edit_published_tribe_venues' => true,
            'delete_published_tribe_venues' => true,
            'delete_private_tribe_venues' => true,
            'edit_private_tribe_venues' => true,
            'read_private_tribe_venues' => true,
            // organizers
            'edit_tribe_organizer' => true,
            'read_tribe_organizer' => true,
            'delete_tribe_organizer' => true,
            'delete_tribe_organizers' => true,
            'edit_tribe_organizers' => true,
            'edit_others_tribe_organizers' => true,
            'delete_others_tribe_organizers' => true,
            'publish_tribe_organizers' => true,
            'edit_published_tribe_organizers' => true,
            'delete_published_tribe_organizers' => true,
            'delete_private_tribe_organizers' => true,
            'edit_private_tribe_organizers' => true,
            'read_private_tribe_organizers' => true,

            // users
            'delete_users' => true,
            'edit_users' => true,
            'list_users' => true,
            'promote_users' => true,
            'create_users' => true,

            // themes
            'edit_theme_options' => true,
            'switch_themes' => true,
            'edit_themes' => true,

            // plugins
            'install_plugins' => false,
            'update_plugin' => false,

            // WordPress core
            'unfiltered_upload' => true,
            'customize' => true,
            'update_core' => false,
            'edit_comment' => true,
            'moderate_comments' => true,
            'manage_options' => true,
        ];

        add_role(DVWP_CLIENT_ADMIN_ROLE, __('Site Administrator'), $capabilities );
    };   
endif;