<?php

/**
 * BJO102Press Classes: ElementorGallery.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

namespace DVWP\Classes;

if (! defined('ABSPATH')) exit;

if (! class_exists('\DVWP\Classes\ElementorGallery')) :
    /**
     * HTML generator class for media gallery which utilizes Elementor functionality.
     *
     * @since 1.0.0
     */
    class ElementorGallery
    {
        /**
         * Media library item ids to loop through when generating the gallery.
         * 
         * @since 1.0.0
         * 
         * @access private
         * @var array
         */
        private $img_ids;

        /**
         * The gallery modal ID.
         * 
         * The string identifyer which defines the which image modal loop these gallery items will appear in.
         * 
         * @since 1.0.0
         * 
         * @access private
         * @var string
         */
        private $galleryID = 'dv_gallery';

        public function __construct($args)
        {
            if(! empty($args['ids']) && is_array($args['ids'])) {
                $this->img_ids = array_filter($args['ids'], function ($img) {
                    return is_numeric($img) && wp_get_attachment_url(intval($img));
                });
            }
            if(isset($args['product_gallery']) && boolval($args['product_gallery']) == true) {
                $this->add_product_gallery();
            }
            if(! empty($args['featured']) && boolval($args['featured']) ) {
                $this->add_featured_img();
            }
            if (isset($args['gallery_id']) && Utils::validate_string($args['gallery_id'], '/^[a-z-_]{1,50}$/') ){
                $this->galleryID = sanitize_text_field( $args['gallery_id']);
            }
        }

        /**
         * Get the media file id of the current post featured image, and add it to the gallery.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         */
        public function add_featured_img() : void
        {
            if (! has_post_thumbnail()) return;
            $this->img_ids[] = get_post_thumbnail_id();
        }

        /**
         * Get media file ids from the product gallery of the current product, and add them to the gallery.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         */
        public function add_product_gallery() : void
        {
            if (! is_product()) return;
            $product = wc_get_product();
            array_push($this->img_ids, ...$product->get_gallery_image_ids());
        }

        /**
         * Return media file id for all gallery items.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return array ids.
         */
        public function get_ids()
        {
            return $this->img_ids;
        }

        /**
         * Manually set/add to media file ids for the gallery.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @param array $ids The ids to add to the array.
         * @param bool $reset Whether previous values should be removed. Defaults to false.
         * @return void
         */
        public function set_ids($ids, $reset = false) : void
        {
            $filtered_imgs = array_filter($ids, function ($id) {
                return is_numeric($id) && wp_get_attachment_url(intval($id));
            });
            if($reset) {
                $this->img_ids = $filtered_imgs;
            } else {
                array_push($this->img_ids, ...$filtered_imgs);
            }
        }

        /**
         * Get the full gallery as HTML.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return string gallery HTML.
         */
        public function get_html()
        {
            $html = '';

            if (count($this->img_ids) > 0) {
                $html .= '<div class="dv-elementor-gallery">';
                foreach ($this->img_ids as $img_id) :
                    $img_id = intval($img_id);
                    $image_title = get_the_title($img_id);
                    $image_alt = get_post_meta($img_id, '_wp_attachment_image_alt', TRUE);
                    $html .= $this->get_gallery_item_html($img_id, $image_title, $image_alt, $this->galleryID);
                endforeach;
                $html .= '</div>';
            } else {
                $html = '<p>No gallery items.</p>';
            }

            return $html;
        }

        /**
         * Generate a gallery item as HTML.
         * 
         * Apply important data as parameters for organizing separate galleries into the same loop in the gallery modal,
         * or specifying important information such as title and caption.
         * 
         * Items of separate galleries on the same page, with the same gallery id, will all appear in the same loop in the
         * Elementor gallery pop-up modal.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @param int $id The media library item ID.
         * @param string $title The title text to appear in the Elementor gallery modal.
         * @param string $alt The caption text to appear in the Elementor gallery modal.
         * @param string $gal_id The text identifyer for the gallery loop.
         * @return string gallery item HTML.
         */
        public static function get_gallery_item_html($id, $title = '', $alt = '', $gal_id = 'dv-gallery')
        {
            $output = '
            <div class="dv-elementor-gallery-item-wrapper">
            <a
            class="dv-elementor-gallery-item-container"
            href="' . wp_get_attachment_url($id) . '"
            data-elementor-open-lightbox="yes"
            data-elementor-lightbox-slideshow="' . esc_attr($gal_id) . '"
            data-elementor-lightbox-title="' . esc_html($title) . '"
            >
                <div
                class="dv-elementor-gallery-item"
                data-thumbnail="' . wp_get_attachment_url($id) . '"
                alt="' . esc_html($alt) . '"
                style="background-image: url(' . wp_get_attachment_url($id) . ');"
                ></div>
                <div class="dv-elementor-gallery-item-overlay"></div>
            </a></div>';

            return $output;
        }
    }
endif;