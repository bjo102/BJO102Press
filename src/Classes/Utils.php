<?php

/**
 * BJO102Press Classes: Utils.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

namespace DVWP\Classes;

if (!defined('ABSPATH')) exit;

if (!class_exists('DVWP\Classes\Utils')) :
    /**
     * The BJO102Press helper class.
     * 
     * Helper functions for other plugins and modules to use.
     * 
     * - Getting primary terms.
     * - Data validation.
     * - Plugin activation checks.
     * 
     * @since 1.0.0
     */
    class Utils
    {
        /**
         * Get the primary term for the post.
         * 
         * Used in conjunction with Yoast SEO plugin for getting the primary term of the current post.
         * This defaults to the first term on the list of get_the_terms() if Yoast is not present.
         * 
         * @since 1.0.0
         * 
         * @param int $id Optional: The ID of the post to get terms from. defaults to the current ID.
         * @param string $tax Optional: The taxonomy to get terms from. Defaults to 'category'. 
         * @return WP_Term[]|false|WP_Error the primary term object
         */
        public static function get_primary_term_obj(int $id = null, string $tax = 'category')
        {
            $term_obj = null;
            $theID = null == $id ? get_the_ID() : intval($id);

            if ($term_obj == null) :
                if (class_exists('WPSEO_Primary_Term')) {
                    $wpseo_primary_term = new \WPSEO_Primary_Term($tax, $theID);
                    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                    $term = get_term($wpseo_primary_term);
                    if (is_wp_error($term)) {
                        return $term_obj = get_the_terms($theID, $tax) ? get_the_terms($theID, $tax)[0] : null;
                    } else {
                        return $term_obj = $term;
                    }
                } else {
                    return $term_obj = get_the_terms($theID, $tax) ? get_the_terms($theID, $tax)[0] : null;
                }
            endif;
        }

        /**
         * Get terms in a given taxonomy, with the primary term as the first term.
         * 
         * @since 1.0.0
         * 
         * @param int $id Optional: The ID of the post to get terms from. defaults to the current ID.
         * @param string $tax Optional: The taxonomy to get terms from. Defaults to 'category'.
         * @param array $args Optional: Arguements to pass when getting terms.
         * @return array|WP_Error The primary term object on success. Null on failure.
         */
        public static function get_the_terms(int $id = null, string $taxonomy = 'category', array $args = [])
        {
            $theID = null == $id ? get_the_ID() : $id;
            $primeTerm = self::get_primary_term_obj($theID, $taxonomy);
            if ($primeTerm) {
                $args = [];
                $args['exclude']  = $primeTerm->term_id;
                $wpTerms          = wp_get_post_terms($theID, $taxonomy, $args);
                $terms            = [...$wpTerms];
                array_unshift($terms, $primeTerm);
            } else {
                $terms = wp_get_post_terms($theID, $taxonomy, $args);
            }

            return $terms;
        }

        /**
         * Get terms in HTML.
         * 
         * A version of get_the_terms() which prioritises the primary term
         * and returns a html template.
         * 
         * @since 1.0.0
         * 
         * @see DV_Utils::get_the_terms()
         * @return string|null $html on success. Null on failure
         */
        public static function get_terms_html(int $id = null, string $taxonomy = 'category', array $args = [])
        {
            $html = '';
            $theID = (null == $id) ? get_the_ID() : $id;
            $terms = self::get_the_terms($theID, $taxonomy, $args);
            if ($terms && !is_wp_error($terms)) {
                $prime_term = $terms[0];
                unset($terms[0]);
                $html .= '<p class="dv-post-taxonomy-terms"><span class="term prime-term">' . esc_html($prime_term->name) . '</span>';
                foreach ($terms as $term) {
                    $html .= '<span class="term">' . esc_html($term->name) . '</span>';
                }
                $html .= '</p>';

                return $html;
            }

            return null;
        }

        /**
         * Hide the empty WooCommerce cart.
         * 
         * This applies the `dv-cart-is-empty` class to the body element of the webpage when the cart is empty.
         * 
         * - The class `dv-cart` can be applied to the cart element/widget/container.
         * - You can use the CSS selector `.dv-cart-is-empty .dv-cart` and set the `display` property to `none`.
         * - The `.dv-cart-is-empty` part will be removed once a shop item has been added to the WC cart.
         * 
         * @since 1.0.0
         * 
         * @return void
         */
        public static function toggle_cart()
        {   
            add_filter('body_class', function ($classes) {
                if (function_exists('WC')) {
                    $cart = WC()->cart;
                    if (isset($cart) && is_callable(array($cart, 'get_cart_contents_count'))) {
                        $items = $cart->get_cart_contents_count();
                        if (!$items) {
                            $classes[] = 'dv-cart-is-empty';
                        }
                    }
                }
                return $classes;
            });

            add_action('wp_enqueue_scripts', function () {
                wp_enqueue_script('dv-toggle-wc-cart');
                wp_localize_script('dv-toggle-wc-cart', 'dvCartData',
                    [
                        'bodyClass' => 'dv-cart-is-empty',
                        'cartClass' => 'dv-cart',
                    ]
                );
            });
        }

        /**
         * Validate a string.
         * 
         * This is a shorthand for a variety of functions for verifying a string.
         * The value is type checked for string before everything else.
         * 
         * @since 1.0.0
         * @since 1.0.1 Re-order parameters and make $maxLen optional.
         * 
         * @param mixed $val The value to be checked.
         * @param string $pattern The Regex to compare the string against.
         * @param int $minLen (Optional) The minimum length of the string. Defaults to null.
         * @param int $maxLen (Optional) The maximum length of the string. Defaults to null.
         * @param bool $trim (Optional) Whether to trim whitespace off the string before validating. Defaults to false.
         * @return bool
         */
        public static function validate_string($val, string $pattern, int $minLen = null, int $maxLen = null, bool $trim = false ): bool
        {
            if (! is_string($val)) {
                return false;
            }
            if ($trim) {
                $val = trim($val);
            }
            if (! preg_match($pattern, $val)) {
                return false;
            }
            if (null != $minLen && strlen($val) < $minLen) {
                return false;
            }
            if (null != $maxLen && strlen($val) > $maxLen) {
                return false;
            }
            
            return true;
        }

        /**
         * Validate an integer.
         * 
         * @since 1.0.0
         * 
         * @param mixed $val The value to be checked.
         * @param int $minLen (Optional) The minimum length of the int. Defaults to null.
         * @param int $maxLen (Optional) The maximum length of the int. Defaults to null.
         * @return bool
         */
        public static function validate_int($val, int $min = null, int $max = null): bool
        {
            $options = [];
            if (null != $min) {
                $options['min_range'] = $min;
            }
            if( null != $max) {
                $options['max_range'] = $max;
            }
            if (! filter_var($val, FILTER_VALIDATE_INT, ['options' => $options])) {
                return false;
            }

            return true;
        }

        /**
         * Validate an array.
         * 
         * @since 1.0.0
         * 
         * @todo Add depth to validation.
         * 
         * @param mixed $val The value to be checked.
         * @return bool
         */
        public static function validate_array($val): bool
        {
            if (! empty($val) && is_array($val)) {
                return true;
            }
            return false;
        }

        /**
         * Validate an IP address.
         * 
         * @since 1.0.0
         * 
         * @param mixed $val The value to be checked.
         * @return bool
         */
        public static function validate_ip($val): bool
        {
            if (filter_var($val, FILTER_VALIDATE_IP)) {
                return true;
            }
            return false;
        }

        /**
         * Validate an object as also being a WP Query.
         * 
         * @since 1.0.1
         * 
         * @param mixed $val The value to be checked.
         * @return bool
         */
        public static function validate_wpquery($query): bool
        {
            if (is_object($query) && get_class($query) != 'WP_Query') {
                return true;
            }
            return false;
        }

        /**
         * Get a general-use BJO102Press notice.
         * 
         * This function accepts the following HMTL as a message.
         * 
         * @since 1.0.0
         * 
         * @param string $msg the HTML to put into the wrapper paragraph element.
         * @param string $class (Optional) The class to apply to the wrapper paragraph element.
         * @return string The notice HTML.
         */
        public static function notice(string $msg = '', string $class = 'dv-notice')
        {
            $allowed = array(
                'a' => [
                    'href' => [],
                    'title' => [],
                    'class' => []
                ],
                'strong' => []
            );
            return '<p class="' . esc_attr($class) . '">' . wp_kses($msg, $allowed) . '</p>';
        }

        /**
         * Print a notice message in the admin screen.
         * 
         * The message is wrapped in a div and p elements.
         * 
         * @since 1.0.0
         * 
         * @param string $msg The message to be printed. Certain HTML elements accepted.
         * @param string $class (Optional). The class applied to the wrapping div. Defaults to notice-warning. 
         * @return void
         */
        public static function admin_notice(string $msg, string $class = 'notice-warning')
        {
            $allowed = array(
                'a' => [
                    'href' => [],
                    'title' => []
                ],
                'strong' => [],
                'button' => [],
                'em' => []
            );
            $theClass = 'notice ' . $class;
            $message = __($msg, 'sample-text-domain');
            printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($theClass), wp_kses($message, $allowed));
        }

        /**
         * Check if a certain plugin is active.
         * 
         * The arguements should represent a path, relative to the plugin folder, which leads to the main file of the plugin in question.
         * 
         * If $file arguement is passed, there's no need to include '.php' this is added automatically.
         * 
         * If $file is ommitted, $folder is concatetenated on itself.
         * 
         * @since 1.0.0
         * 
         * @param string $folder The folder name of the plugin to 
         * @param string $file (Optional) The main file of the plugin to check.
         * @return bool
         */
        public static function is_plugin_active(string $folder, string $file = null)
        {
            $path = $folder . '/';
            $path .= ($file != null) ? $file . '.php' : $folder . '.php';
            $plugins = apply_filters('active_plugins', get_option('active_plugins'));
            if (in_array($path, $plugins)) {
                return true;
            }

            return false;
        }

        /**
         * Prepare the admin notice for missing dependencies.
         * 
         * @since 1.0.1
         * 
         * @return void
         */
        public static function missing_deps_notice($pluginName, $deps)
        {
            $allowed = array(
                'p' => [],
                'ul' => [],
                'li' => [],
                'strong' => [],
            );
            $msg = '<p>' . $pluginName . ' relies on the following plugins to function:</p><ul>';
            foreach ($deps as $dep) {
                $msg .= '<li><strong>' . esc_html($dep) . '</strong></li>'; 
            }
            $msg .= '</ul>';
            $theClass = 'notice notice-error';
            $message = __($msg, 'sample-text-domain');

            printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($theClass), wp_kses($message, $allowed));
        }

        /**
         * Make a string URL friendly.
         *
         * @since 1.0.1
         * 
         * @param string $str The string to slugify.
         * @return string The slugified string.
         */
        public static function slugify_string(string $str) : string
        {
            $str = preg_replace('/^\s+|\s+$/m', '', $str);
            $str = strtolower($str);

            // remove accents, swap ñ for n, etc
            $accentsAlt = array(
                'à' => 'a',
                'á' => 'a',
                'ä' => 'a',
                'â' => 'a',
                'è' => 'e',
                'é' => 'e',
                'ë' => 'e',
                'ê' => 'e',
                'ì' => 'i',
                'í' => 'i',
                'ï' => 'i',
                'î' => 'i',
                'ò' => 'o',
                'ó' => 'o',
                'ö' => 'o',
                'ô' => 'o',
                'ù' => 'u',
                'ú' => 'u',
                'ü' => 'u',
                'û' => 'u',
                'ñ' => 'n',
                'ç' => 'c',
                '·' => '-',
                '/' => '-',
                '_' => '-',
                ',' => '-',
                ':' => '-',
                ';' => '-',
            );

            $str = strtr($str, $accentsAlt); // no more accents.
            $str = preg_replace('/[^a-z0-9 -]/m', '', $str); // remove invalid chars
            $str = preg_replace('/\s+/m', '-', $str); // collapse whitespace and replace by -
            $str = preg_replace('/-+/m', '-', $str); // collapse dashes
            $str = preg_replace('/^-+/', '', $str); // trim - from start of text
            $str = preg_replace('/-+$/', '', $str); // trim - from end of text

            return $str;
        }
        
        /**
         * Get DOM document and nodes via XPath query, and bind the nodes with anchors.
         * 
         * This is a helper function for dv_post_single_navigation shortcode.
         * 
         * @since 1.0.1
         * 
         * @see DV_Utils::get_dom_nodes()
         * @param $content The content to be parsed.
         * @param string $query The XPath query to be used to pull nodes from the content DOM.
         * @return object The data object, with a property for "dom" and "nodes" - each with a "node" and "anchor" property.
         */
        public static function get_prepared_dom_achors($content, string $query) : object
        {
            /** @var object */
            $domData = Utils::get_dom_nodes($content, $query);
            
            if (! empty($domData->nodes)) :
                $i = 0;
                $prevAnchors = [];
                $nodesData = [];
                foreach($domData->nodes as $node) :
                    $anchor = Utils::slugify_string($node->textContent);
                    if(in_array($anchor, $prevAnchors)) {
                        $anchor .= '-node-index-' . strval($i);
                    }
                    $nodeObj = new \stdClass;
                    $nodeObj->node = $node;
                    $nodeObj->anchor = $anchor;
                    $nodesData[] = $nodeObj;
                    array_push($prevAnchors, $anchor);
                    $i++;
                endforeach;
                $domData->nodes = $nodesData;
            endif;

            return $domData;
        }

        /**
         * Get DOM elements from the specified content via XPath query.
         * 
         * This will return an object containing the nodes, and the DOMDocument used to load the content before parsing with the query,
         * this will allow you to apply changes to the nodes pulled from the content if you wish to.
         * 
         * @since 1.0.0
         * @since 1.0.1 Use XPath queries instead of regexes and for loops to get content. Add $query parameter.
         * 
         * @param $content The content to be parsed.
         * @param string $query The XPath query to be used to pull nodes from the content DOM.
         * @return object The data object, with a property for "dom" and "nodes".
         */
        public static function get_dom_nodes($content, string $query) : object
        {
            $content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');
            libxml_use_internal_errors(true);

            $dom = new \DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->validateOnParse = true;
            $dom->loadHTML(utf8_decode($content));
            
            $xpath = new \DOMXPath($dom);
            $nodes = $xpath->query($query);

            $data = new \stdClass;
            $data->dom = $dom;
            $data->nodes = $nodes;

            return $data;
        }
    }
endif;
