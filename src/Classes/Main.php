<?php

/**
 * BJO102Press Classes: Main.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

namespace DVWP\Classes;
use function DVWP\UserRoles\InitClientRole;

if (! defined('ABSPATH')) exit;

if (! class_exists('DVWP\Classes\Main')) :
    /**
     * The BJO102Press main class.
     * 
     * Pulls all the required files and sets adds essential actions and filters.
     * The tasks of this class include the following:
     * 
     * - Ajax hooks for DV_Posts. 
     * - Custom query variables.
     * - The Site Admin user role.
     * - Adminbar customisations.
     * - Base styles and scripts.
     * - Admin footer text.
     *
     * @since      1.0.3
     */
    class Main
    {
        public function __construct()
        {
            new Utils;
            new Admin;
            new Shortcodes;

            $this->loadDeprecated();

            add_action('init',                          [$this, 'init']);
            add_action('wp_enqueue_scripts',            [$this, 'enqueueScripts'], 1);
            add_filter('script_loader_tag',             [$this, 'editScriptTags'], 10, 3);
            add_filter('query_vars',                    [$this, 'setQVars']);
            add_filter('the_content',                   [$this, 'postSingleNavHTMLSetup'], 1);

            Posts::addAjaxActions();
        }

        /**
         * Load deprecated services.
         * 
         * @since 1.0.3
         * 
         * @return void
         */
        private function loadDeprecated()
        {
            require_once DVWP_PLUGIN_PATH . 'src/Classes/Deprecated/DVWP.php';
            require_once DVWP_PLUGIN_PATH . 'src/Classes/Deprecated/DvElementorGallery.php';
            require_once DVWP_PLUGIN_PATH . 'src/Classes/Deprecated/DvUtils.php';
            require_once DVWP_PLUGIN_PATH . 'src/Classes/Deprecated/DvPosts.php';
        }

        /**
         * Run actions for the init hook.
         * 
         * @since 1.0.3
         * 
         * @access public
         * @return void
         * @todo Re-introduce `clientRoleCapabilities` when `manage_options` capability has been set back to false in the Client Role specification. This has only been granted to them due to special requests, this needs to be changed as they now have access to general settings, and they could break the site.
         */
        public function init()
        {
            InitClientRole();
            // $this->clientRoleCapabilities();
        }

        /**
         * Print scripts and css on the front end,
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         */
        public static function enqueueScripts()
        {
            // css enqueues
            wp_enqueue_style(
                'dynamicalview-wp',
                DVWP_PLUGIN_URL . 'public/css/style.css'
            );

            // js registations
            wp_register_script(
                'dv-posts-ajax',
                DVWP_PLUGIN_URL . 'public/js/posts-ajax.js',
                ['jquery'],
                '1.0',
                true
            );
            wp_register_script(
                'dv-toggle-wc-cart',
                DVWP_PLUGIN_URL . 'public/js/toggle-wc-cart.js',
                ['jquery'],
                '1.0',
                true
            );
            wp_register_script(
                'dv-post-single-navigation',
                DVWP_PLUGIN_URL . 'public/js/post-single-navigation.js',
                ['jquery'],
                '1.0',
                true
            );

            // js enqueues
            wp_enqueue_script(
                'dv-modal-menu-action',
                DVWP_PLUGIN_URL . 'assets/js/modal-menu-action.js',
                ['jquery'],
                '1.0',
                true
            );
        }

        /**
         * Alter script tags for certain handles.
         * 
         * This includes turning scripts into modules via HTML attribute.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @link https://developer.wordpress.org/reference/hooks/script_loader_tag/
         * @return string The script tag HTML.
         */
        public function editScriptTags($tag, $handle, $src)
        {
            $modules = [
                'dv-admin-settings',
                'dvMedialModal'
            ];
            if (in_array($handle, $modules)) {
                $tag = '<script type="module" src="' . esc_url($src) . '" id="dropboxjs" data-app-key="MY_APP_KEY"></script>';
            }

            return $tag;
        }

        /**
         * Set query variabls for page, sort and all public registered taxonomies.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @param array $vars WordPress query variables.
         * @return array The amended query variables.
         */
        public static function setQVars($vars)
        {
            $vars[] = 'dv_page';
            $vars[] = 'dv_sort';
            $vars[] = 'dv_search';
            foreach (get_taxonomies(['public' => true]) as $tax) {
                $vars[] = 'dv_' . $tax;
            }

            return $vars;
        }

        /**
         * Prepare content for dv post single navigation links.
         * 
         * This involves getting specified DOM nodes and applying custom data attributes
         * to them, each with unique values. These values are used later for scroll-to events.
         * 
         * @since 1.0.1
         * 
         * @access public
         * @return mixed The content.
         */
        public static function postSingleNavHTMLSetup($content)
        {
            if (is_singular() && in_the_loop() && is_main_query()) {
                /** @var object */
                $domData = Utils::get_prepared_dom_achors($content, '//h2|//h3');
                $dom = $domData->dom;
                $nodes = $domData->nodes;

                foreach ($nodes as $nodeObj) {
                    /** Node Element */
                    $nodeEl = $nodeObj->node;
                    $nodeAnchor = $nodeObj->anchor;

                    $nodeEl->setAttributeNode(new \DOMAttr('data-' . DVWP_CUSTOM_HTML_ANCHOR, $nodeAnchor));
                    $nodeEl->setIDAttribute('data-' . DVWP_CUSTOM_HTML_ANCHOR, true);
                }

                return $content = $dom->saveHTML();
            }

            return $content;
        }

        /**
         * Give a site administrator special privileges when they are on certain pages.
         * 
         * @since 1.0.2
         * @since 1.0.3 change method name from "siteAdminCustomizerCap" to "clientRoleCapabilities".
         * 
         * @return void
         */
        private function clientRoleCapabilities()
        {
            $user = wp_get_current_user();
            $roles = (array) $user->roles;

            if (!in_array(DVWP_CLIENT_ADMIN_ROLE, $roles)) {
                return;
            }

            if (is_customize_preview()) {
                $user->add_cap('manage_options');
            } else if ($user->has_cap('manage_options')) {
                $user->remove_cap('manage_options');
            }
        }

        public static function uninstall() {}
    }
endif;
