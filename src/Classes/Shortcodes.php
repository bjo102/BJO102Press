<?php

/**
 * BJO102Press Classes: Shortcodes.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

namespace DVWP\Classes;
use function DVWP\Shortcodes\Posts;
use function DVWP\Shortcodes\PostSingleNav;
use function DVWP\Shortcodes\ElementorGallery;

if (! defined('ABSPATH')) exit;

if (! class_exists('DVWP\Classes\Shortcodes')) :
    /**
     * A class for adding shocodes native to BJO102Press.
     *
     * @since 1.0.0
     */
    class Shortcodes
    {
        /**
         * Add shortcodes to the system with functions of this class.
         * 
         * @since 1.0.0
         * @since 1.0.3 Turn the "shortcodesInit" method into the class constructor.
         * 
         * @return void
         */
        public function __construct()
        {
            add_shortcode('dv_posts'                    , [$this, 'posts']);
            add_shortcode('dv_post_single_navigation'   , [$this, 'postSingleNav']);
            add_shortcode('dv_elementor_gallery'        , [$this, 'elementorGallery']);
        }

        public static function posts($atts)
        {
            return Posts($atts);
        }

        public static function postSingleNav($atts)
        {
            return PostSingleNav($atts);
        }

        public static function elementorGallery($atts)
        {
            return ElementorGallery($atts);
        }

        /**
         * Break attribute string by seperator.
         * 
         * @since 1.0.1
         * @since 1.0.3 Make this method public.
         * 
         * @access private
         * @return array The broken string.
         */
        public static function breakAttr(string $string) : array
        {
            $no_whitespaces = preg_replace('/\s*,\s*/', ',', sanitize_text_field($string));
            $items = explode(',', $no_whitespaces);

            return $items;
        }
    }
endif;
