<?php

/**
 * BJO102Press Classes: DV_Admin.
 * 
 * @since 1.0.2
 * 
 * @package BJO102Press\Classes
 */

namespace DVWP\Classes;

if (!defined('ABSPATH')) exit;

if (!class_exists('DVWP\Classes\Admin')) :
    /**
     * The Administration class.
     * 
     * This applies templates, scripts and settings pages to the admin screen.
     * Some properties seem to be unused in this file, but are used in required template files.
     * 
     * @since 1.0.2
     */
    class Admin
    {

        /**
         * The default wrapper link for the login logo.
         * 
         * @since 1.0.2
         * 
         * @var string
         */
        private $defaultLogoLink = DVWP_SITE_URL;

        /**
         * The default login logo image URL.
         * 
         * @since 1.0.2
         * 
         * @var string
         */
        private $defaultLogoFile = DVWP_LOGO_URL;

        /**
         * The enabled status for admin settings.
         * 
         * @since 1.0.3
         * 
         * @var boolean
         */
        private $adminSettingsEnabled;

        /**
         * An accociative array containing option keys used by BJO102Press.
         * 
         * @since 1.0.2
         * 
         * @var array
         */
        private $options = [
            'admin_styles_enabled'              => 'dvwp_admin_styles_enabled',
            'login_logo_img'                    => 'dvwp_login_logo_image',
            'login_logo_link'                   => 'dvwp_login_logo_link',
            'login_message'                     => 'dvwp_login_message',
            'login_submit_button_colour'        => 'dvwp_login_submit_button_colour',
            'login_submit_font_colour'          => 'dvwp_login_submit_font_colour',
            'login_submit_font_hover_colour'    => 'dvwp_login_submit_font_hover_colour',
            'login_submit_button_hover_colour'  => 'dvwp_login_submit_button_hover_colour',
            'login_background_colour'           => 'dvwp_login_background_colour',
            'login_accent'                      => 'dvwp_login_accent',
            'admin_bar_logo_img'                => 'dvwp_admin_bar_logo_img',
            'admin_bar_logo_link'               => 'dvwp_admin_bar_logo_link',
            'admin_bar_logo_alt'                => 'dvwp_admin_bar_logo_alt',
        ];

        /**
         * Option data gathere
         * 
         * @since 1.0.2
         * 
         * @var array
         * @see DV_Admin::getOptionsData()
         */
        public $oData;

        public function __construct()
        {
            $this->oData = $this->getOptionsData();
            $this->adminSettingsEnabled = $this->oData['admin_styles_enabled']['value'];

            add_action('admin_footer',                  [$this, 'enqueueAdminScripts']);
            add_action('wp_before_admin_bar_render',    [$this, 'adminBarItems']);
            add_action('admin_menu',                    [$this, 'registerAdminSettingsPages']);
            add_action('login_enqueue_scripts',         [$this, 'loginScripts']);
            add_action('wp_before_admin_bar_render',    [$this, 'reorderAdminBar']);
            add_filter('wpseo_metabox_prio',            [$this, 'yoastToBottom']);
            add_filter('admin_footer_text',             [$this, 'adminFooterNotice']);
            add_filter('login_headerurl',               [$this, 'loginLogoUrl']);
            add_filter('login_message',                 [$this, 'loginMessage']);

            //add_action('wp_enqueue_scripts',            [$this, 'adminBarLogo']);
            //add_action('login_enqueue_scripts',         [$this, 'adminBarLogo']);
            //add_action('admin_enqueue_scripts',         [$this, 'adminBarLogo']);
            //register_activation_hook( __FILE__, 'cd_wp_branding_activate' );
        }

        /**
         * Enqueue BJO102Press admin scripts.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return void
         */
        public function enqueueAdminScripts()
        {
            // css enqueues
            wp_enqueue_style(
                'dv-admin',
                DVWP_CSS_URL . 'admin.css'
            );

            wp_register_script(
                'dv-admin-settings',
                DVWP_JS_URL . 'admin-settings.js',
                ['jquery'],
                '1.0',
                true
            );
            wp_localize_script('dv-admin-settings', 'dvwpAdminOptions', $this->getOptionsData());
            wp_enqueue_script('dv-admin-settings');
        }

        /**
         * Add/Remove admin bar items.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @global $wp_admin_bar
         * @param object $wp_admin_bar
         * @link https://wordpress.stackexchange.com/questions/125997/how-can-i-specify-the-position-of-an-admin-bar-item-added-with-wp-admin-bar-ad
         * @return void
         */
        public function adminBarItems($wp_admin_bar)
        {
            global $wp_admin_bar;

            $adminBarImgUrl = wp_get_attachment_url(get_option($this->options['admin_bar_logo_img']));
            $adminMenuBarLink = get_option($this->options['admin_bar_logo_link']);
            $adminMenuBarAltText = get_option($this->options['admin_bar_logo_alt']);

            // add custom admin bar logo/image
            if ($adminBarImgUrl && $this->adminSettingsEnabled) {
                $args = array(
                    'id' => 'dvwp-admin-logo',
                    'href' => $adminMenuBarLink,
                    'title' => sprintf('<img src="%s" />', $adminBarImgUrl),
                    'meta' => array(
                        'class' => 'dvwp-admin-logo',
                        'title' => $adminMenuBarAltText,
                        'target' => '_blank',
                        'style' => 'max-width: 50px;'
                    )
                );
                $wp_admin_bar->add_node($args);
            }

            // remove WordPress logo
            $wp_admin_bar->remove_node('wp-logo');
        }

        /**
         * Get formatted data for options.
         * 
         * This includes html attributes for form fields and is used on settings pages and JavaScript selectors.
         * 
         * @since 1.0.2
         * 
         * @return array the formatted data array items for each option.
         */
        private function getOptionsData()
        {
            $theData = [];
            foreach ($this->options as $key => $realKey) {
                $theData[$key] = [
                    'key' => $realKey,
                    'value' => get_option($realKey),
                    'id' => preg_replace('/_/', '-', $realKey)
                ];
            };
            return $theData;
        }

        /**
         * Re-order admin bar items before rendering.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @global $wp_admin_bar
         * @return void
         */
        public function reorderAdminBar()
        {
            global $wp_admin_bar;

            // The desired order of identifiers (items)
            $IDs_sequence = array(
                'dvwp-admin-logo',
                'wp-logo',
                'site-name',
                'new-content',
                'edit'
            );

            // Get an array of all the toolbar items on the current page
            $nodes = $wp_admin_bar->get_nodes();

            // Perform recognized identifiers
            foreach ($IDs_sequence as $id) {
                if (!isset($nodes[$id])) continue;

                // This will cause the identifier to act as the last
                // menu item
                $wp_admin_bar->remove_menu($id);
                $wp_admin_bar->add_node($nodes[$id]);

                // Remove the identifier from the list of nodes
                unset($nodes[$id]);
            }

            // Unknown identifiers will be moved to appear after known
            // identifiers
            foreach ($nodes as $id => &$obj) {
                // There is no need to organize unknown children
                // identifiers (sub items)
                if (!empty($obj->parent)) continue;

                // This will cause the identifier to act as the last
                // menu item
                $wp_admin_bar->remove_menu($id);
                $wp_admin_bar->add_node($obj);
            }
        }

        /**
         * Enqueue/load assets for the login screen.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return void
         */
        public function loginScripts()
        {
            require_once DVWP_VIEWS_PATH . 'Login.php';
        }

        /**
         * Filter the text that appears after the logo on the login form.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return string The login form message.
         */
        public function loginMessage()
        {
            if ($this->adminSettingsEnabled) {
                return get_option($this->options['login_message']);
            }
        }

        /**
         * Return the wp options declared by the DV_Admin class.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return void
         */
        public function getOptions()
        {
            return $this->options;
        }

        /**
         * Register DVWP admin settings pages.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return void
         */
        public function registerAdminSettingsPages()
        {
            add_submenu_page(
                'options-general.php',
                'DVWP settings',
                'DVWP settings',
                'manage_options',
                'dvwp_admin_settings_main',
                [$this, 'renderMainAdminSettingsPage']
            );
        }

        /**
         * Render the main Dynamical WP admin settings page.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return void
         */
        public function renderMainAdminSettingsPage()
        {
            require_once DVWP_VIEWS_PATH . 'AdminSettingsMain.php';
        }

        /**
         * Override the link used for the logo on the login screen.
         * 
         * In the case where the login logo link is not set, the logo image is checked as a condition for the default link to be used.
         * 
         * If the logo image is set, the home url is used as the link. If the logo image is not set, the BJO102Press logo is used as the image, so the DV website is used as the link.
         * 
         * @since 1.0.2
         * 
         * @access public
         * @return string The Logo URL.
         */
        public function loginLogoUrl($url)
        {
            if (! $this->adminSettingsEnabled) {
                return $this->defaultLogoLink;
            }
            $optVals = $this->getOptionsData();
            $loginLogoURL = $optVals['login_logo_link']['value'];
            $loginLogoImg = $optVals['login_logo_img']['value'];

            if ($loginLogoURL != '') {
                return $loginLogoURL;
            } else {
                if ($loginLogoImg) {
                    return home_url();
                } else {
                    return $this->defaultLogoLink;
                }
            }
        }

        /**
         * Admin footer customisation.
         * 
         * Add footer notice at the bottom of the admin screen with the
         * Cloud Daemons/Dynamcial View branding.
         * 
         * @since 1.0.0
         * @since 1.0.2 Restrict content to standard html elements.
         * 
         * @access public
         * @return string
         */
        public static function adminFooterNotice()
        {
            $txt = 'Thank you for choosing <strong>BJO102Press</strong>';
            $txt = apply_filters('dv_admin_footer_text', $txt);
            $allowed = [
                'strong' => []
            ];

            return '
            <a id="cd-admin-footer-logo-container" title="Cloud Daemons" href="' . esc_url(DVWP_SITE_URL) . '" target="_blank">
                <img id="cd-admin-footer-logo" src="' . esc_url(DVWP_LOGO_URL) . '" alt="Cloud Daemons Logo" width="35">
                <span id="cd-admin-footer-logo-text">' . wp_kses($txt, $allowed) . '</span>
            </a>';
        }

        /**
         * Load an admin settings form filed container.
         * 
         * This should be used to keep HTML consistent.
         * 
         * @since 1.0.3
         * 
         * @param string $label The label text.
         * @param string $id The "for" attribute for the label and the "id" attribute for the input.
         * @param string $name The "name" attribute for the input.
         * @param string $val The "value" attribute for the input.
         * @param string $type The "type" attribute for the input.
         * @param string $before The content to appear before the input.
         * @param string $after The content to appear after the input.
         * @return mixed The field container HTML.
         */
        public static function settingsFormField($label, $id, $name, $val, $type = 'text', $before = '', $after = '')
        {
            require DVWP_PLUGIN_PATH . 'src/Views/SettingsFormField.php';
        }

        /**
         * Set the position of yoast settings on the post editing screen.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         */
        public static function yoastToBottom()
        {
            return 'low';
        }
    }
endif;
