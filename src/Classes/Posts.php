<?php

/**
 * BJO102Press Classes: Archive.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

namespace DVWP\Classes;

if (!defined('ABSPATH')) exit;

if (!class_exists('\DVWP\Classes\Posts')) :
    /**
     * A class for producing post archives.
     * 
     * One instance of this class represents an archive. There are 
     * methods for getting html templates for posts, taxonomy filters, sort-order
     * and search boxes.
     * 
     * 
     * @since 1.0.0
     */
    class Posts
    {
        /**
         * The prefix used for the query variables of the archive.
         * 
         * @since 1.0.0
         * 
         * @var string
         * @todo This property is not yet in use.
         */
        public $prefix = 'dv';

        /**
         * The WP Query object.
         * 
         * @since 1.0.1
         * 
         * @var object
         */
        private $wpQuery = null;

        /**
         * The arguements to be used in WP_Query for the archive.
         * 
         * @since 1.0.0
         * 
         * @var array
         */
        private $wpQArgs = array();

        /**
         * The array of the query variables for this archive.
         * 
         * This a multidimensional array containing keys and values.
         * if the archive taxonomies are set, one of the items is a taxonomies array.
         * 
         * @since 1.0.0
         * @since 1.0.1 Make this a multidimensional array, each item having a key and value.
         * 
         * @var array The query vars.
         */
        private $qVars = array();

        /**
         * Taxonomy specification for ajax filter links.
         * 
         * @since 1.0.0
         * 
         * @var array
         */
        private $taxonomies = array();

        /**
         * The backup image used for posts without a featured image.
         * 
         * @since 1.0.0
         * 
         * @var int|null
         */
        private $bkpImg = null;

        /**
         * Check for the presense and validity of specific arguements which would serve as settings for this archive.
         */
        public function __construct(array $settings = array())
        {
            if (isset($settings['wp_query'])) {
                $this->setWpQuery($settings['wp_query']);
            }

            if (isset($settings['wp_query_args'])) {
                $this->setWpQArgs($settings['wp_query_args']);
            }

            if (isset($settings['taxonomies'])) {
                $this->setTaxonomies($settings['taxonomies']);
            }

            if (isset($settings['use_qvars']) && boolval($settings['use_qvars']) == true) {
                $this->setQVars();
            }

            if (isset($settings['backup_img'])) {
                $this->bkpImg = filter_var($settings['backup_img'], FILTER_SANITIZE_NUMBER_INT);
            }
        }

        /**
         * Get the WP Query of this archive.
         * 
         * @since 1.0.1
         * 
         * @access public
         * @return object WP Query object.
         */
        public function getWpQuery()
        {
            return $this->wpQuery;
        }

        /**
         * Set the WP Query of this archive.
         * 
         * @since 1.0.1
         * 
         * @access public
         * @return void
         */
        public function setWpQuery(object $q)
        {
            if (get_class($q) != 'WP_Query') {
                throw new \Exception('DV_Posts::setWpQuery($q): $q is not an instance of the WP_Query class.');
            }
            $this->wpQuery = $q;
        }

        /**
         * Get the WP Query for this archive.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return array WP Query args.
         */
        public function getWpQArgs()
        {
            return $this->wpQArgs;
        }

        /**
         * Set the WP Query for this archive.
         * 
         * Checks for the presense and validity of specific arguements which would
         * serve as wp query args settings for this archive. Some settings have query variable
         * fallbacks if they are not set.
         * 
         * @since 1.0.0
         * @since 1.0.1 Remove checks on array items.
         * 
         * @access public
         * @uses filter dv_posts_wp_args
         * @param array $wpQArgs The supplied settings for the WP Query.
         * @return void
         */
        public function setWpQArgs(array $wpQArgs = array())
        {
            /**
             * Filter the WP Query arguements of the current archive.
             * 
             * @example
             * 
             *     add_filter('dv_posts_wp_args', 'dvPostsWpArgs');
             *     public function dvPostsWpArgs($wpArgs)
             *     {
             *         if ($wpArgs['post_type'] == 'tribe_events') {
             *             $wpArgs['meta_query'][] = [
             *                 'key' => '_EventStartDate',
             *                 'value'   => get_option('edwp_programme_min_date'),
             *                 'compare' => '>=',
             *             ];
             *         }
             *         return $wpArgs;
             *     }
             * 
             * @since 1.0.0
             * 
             * @param array $wpArgs Accociative array containing default array items for the archvie WP Query.
             */
            $filteredWpQArgs = apply_filters('dv_posts_wp_args', $wpQArgs);
            $this->wpQArgs = $filteredWpQArgs;
        }

        /**
         * Get the query variables for this archvie.
         * 
         * @since 1.0.1
         * 
         * @access public
         * @return void
         */
        public function getQVars()
        {
            return $this->qVars;
        }

        /**
         * The scheme for items in the query variables array.
         * 
         * @since 1.0,1
         * 
         * @access private
         * @return array the key and value of the query variable.
         * @see DV_Posts::qVars
         */
        private function qVarTemp(string $v): array
        {
            $qVar = $this->prefix . '_' . $v;
            $qVarTemp = [
                'key' => $qVar,
                'value' => get_query_var($qVar)
            ];
            return $qVarTemp;
        }

        /**
         * Set the query variables for this archvie.
         * 
         * This registers query variables for taxonomy filters, so this should be be run
         * after taxonomies have been set.
         * 
         * Taxonomy query varibales should already be set by the `DVWP` class at this point.
         * 
         * @since 1.0.1
         * 
         * @access public
         * @return void
         * @see DV_Posts::setTaxonomies()
         * @see DVWP::setQVars
         */
        public function setQVars()
        {
            $this->qVars['page'] = $this->qVarTemp('page');
            $this->qVars['sort'] = $this->qVarTemp('sort');
            $this->qVars['search'] = $this->qVarTemp('search');

            if (!empty($this->taxonomies)) {
                $this->qVars['taxonomies'] = [];
                foreach ($this->taxonomies as $tax) {
                    if (!isset($this->qVars['taxonomies'][$tax])) {
                        $this->qVars['taxonomies'][$tax] = $this->qVarTemp($tax);
                    }
                }
            }
        }

        /**
         * Apply query variables to wp query arguements.
         * 
         * @since 1.0.1
         * 
         * @access private
         * @return void
         */
        private function applyQVars()
        {
            if (empty($this->qVars)) return;

            $this->applyPagedQVar();
            $this->applySearchQVar();
            $this->applySort();
            $this->applyTaxonomies();
        }

        /**
         * Apply the "paged" query variable to WP Query arguements.
         * 
         * @since 1.0.1
         * 
         * @access private
         * @return void
         */
        private function applyPagedQVar()
        {
            if (!empty($this->qVars['page']['value'])) {
                $this->wpQArgs['paged'] = $this->qVars['page']['value'];
            } else {
                $this->wpQArgs['paged'] = 1;
            }
        }

        /**
         * Apply the "search" query variable to WP Query arguements.
         * 
         * @since 1.0.1
         * 
         * @access private
         * @return void
         */
        private function applySearchQVar()
        {
            if (!empty($this->qVars['search']['value'])) {
                $this->wpQArgs['s'] = $this->qVars['search']['value'];
            }
        }

        /**
         * Apply taxonomy query variables to the WP Query arguments.
         * 
         * @since 1.0.1
         * 
         * @access private
         * @return void
         */
        private function applyTaxonomies(): void
        {
            if (empty($this->qVars['taxonomies'])) return;


            $dfltRelation = 'AND';
            $taxVars = $this->qVars['taxonomies'];
            $taxQuery = [];

            foreach ($this->taxonomies as $tax) :
                $taxVal = $taxVars[$tax]['value'];
                if (isset($taxVal) && null != $taxVal) {
                    $taxQuery[] = [
                        'taxonomy' => $tax,
                        'field'    => 'slug',
                        'terms'    => $taxVal,
                        'operator' => 'IN',
                    ];
                }
            endforeach;

            $this->wpQArgs['tax_query'] = $taxQuery;
            $this->wpQArgs['tax_query']['relation'] = $dfltRelation;
        }

        /**
         * Return the taxonomies set for this archive.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return array The taxonomies.
         * @see DV_Posts::setTaxonomies()
         */
        public function getTaxonomies()
        {
            return $this->taxonomies;
        }

        /**
         * Set the taxonomies for this archive.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @param array $taxonomies Taxonomy slugs.
         * @param bool $push Whether or not you want to keep previously set taxonomies.
         * @return void
         */
        public function setTaxonomies(array $taxonomies, bool $push = false): void
        {
            if ($push == false) {
                $this->taxonomies = [];
            }

            foreach ($taxonomies as $tax) {
                $cleanTax = sanitize_text_field($tax);
                $realTax = get_taxonomy($cleanTax);
                if ($realTax) {
                    if (!isset($this->taxonomies[$tax])) {
                        $this->taxonomies[] = $tax;
                    }
                    if (isset($this->qVars['taxonomies']) && !isset($this->qVars['taxonomies'][$tax])) {
                        $this->qVars['taxonomies'][$tax] = $this->qVarTemp($tax);
                    }
                }
            }
        }

        /**
         * Echo the archive HTML.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         * @see DV_Posts::getArchive()
         */
        public function archive()
        {
            echo $this->getArchive();
        }

        /**
         * Get the archive HTML.
         * 
         * Returns the container element for posts, and the Ajax response.
         * This also localises the accociated JavaScript data.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return string archive HTML in string format.
         * @see DV_Posts::getArchive()
         */
        public function getArchiveContainer()
        {
            $hmtl = '';

            $hmtl .= '<div class="dv-posts">'; // Ajax filter container
            $hmtl .= $this->getArchive();
            $hmtl .= '</div>';

            $localObj = [
                'ajaxUrl' => admin_url('admin-ajax.php'),
                'prefix' => $this->prefix,
                'backupImg' => $this->bkpImg,
                'taxonomies' => $this->taxonomies,
                'wpQArgs' => $this->wpQArgs,
            ];

            wp_localize_script(
                'dv-posts-ajax',
                'dvAjaxObj',
                $localObj
            );
            wp_enqueue_script('dv-posts-ajax');

            return $hmtl;
        }

        /**
         * Run the archive loop.
         * 
         * Generates HTML for each item in the loop.
         * 
         * @since 1.0.0
         * 
         * @access private
         * @uses filter dv_posts_post_details
         * @return string archive posts loop HTML in string format.
         */
        private function getArchive(object $query = null)
        {
            $html = '';

            if (!empty($this->wpQArgs)) {
                $this->applyQVars();
                $query = new \WP_Query($this->wpQArgs);
            } else {
                return Utils::notice('<strong>No Query has been set.</strong>');
            }

            // check loop
            if ($query->have_posts()) :

                $posts_per_page = $this->wpQArgs['posts_per_page'];
                $page_number_max = ceil($query->found_posts / $posts_per_page);

                // total number of posts and current page
                $html .= '<div class="query-pages-notices">';
                $html .= '<p class="total-pages">' . $query->found_posts . ' Items</p>';
                $html .= '<p class="current-page">Page ' . $query->query_vars['paged'] . ' of ' . $page_number_max . '</p>';
                $html .= '</div>';

                // the loop
                while ($query->have_posts()) : $query->the_post();

                    $id = get_the_ID();

                    // the post
                    $html .= '<article id="' . get_post_field('post_name', get_post()) . '-' . esc_attr($id) . '" class="dv-post">';

                    // the thumbnail
                    if (has_post_thumbnail()) {
                        $html .= '<a href="' . get_the_permalink() . '" class="dv-post-link dv-post-image-link">' . get_the_post_thumbnail($id, 'full') . '</a>';
                    } else if (null != $this->bkpImg) {
                        $html .= '<a href="' . get_the_permalink() . '" class="dv-post-link dv-post-image-link">' . wp_get_attachment_image($this->bkpImg, 'full') . '</a>';
                    }

                    // start post details
                    $postDetails = [
                        'categories' => \DV_Utils::get_terms_html($id, 'category'),
                        'excerpt' => get_the_excerpt() ? '<p class="dv-post-excerpt">' . get_the_excerpt() . '</p>' : '',
                        'read_more' => '<a href="' . get_the_permalink() . '" class="read-more dv-post-link">Read More</a>',
                    ];
                    $html .= '<div class="post-details">';
                    $html .= '<a href="' . get_the_permalink() . '" class="dv-post-link dv-post-heading-link"><h4 class="event_heading">' . get_the_title() . '</h4></a>';

                    /**
                     * Filter post metadata on DV Posts archives.
                     *
                     * Set up what kind of data is displayed for each post when it appears in the archive.
                     * 
                     * Example:
                     * 
                     *     add_filter('dv_posts_post_details', 'getPostsData');
                     *     function getPostsData($details, $id, $postType) {
                     *         if ($postType == 'tribe_events') {
                     *             $details = [
                     *                 'start_date' => tribe_get_start_date($id, false, 'd/m/y'),
                     *                 'event_price_dates' => '<div class="dv-event-price-dates"><p>' . esc_html(tribe_get_cost($id, true)) . '</p>' . DV_EVENT::get_dates($id, 'd/m/y') . '</div>',
                     *                 'event_categories' => DV_Utils::get_terms_html($id, 'tribe_events_cat'),
                     *             ];
                     *         }
                     *         return $details;
                     *     }
                     *
                     * @since 1.0.0
                     * 
                     * @param array $postDetails Accociative array containing default sequence and values to be shown for each post in the archive.
                     * @param int $id The current post in the loop.
                     * @param string $postType The post type of the current post.
                     */
                    $postDetails = apply_filters('dv_posts_post_details', $postDetails, $id, get_post_type());

                    foreach ($postDetails as $postDetail) {
                        $html .= $postDetail;
                    }

                    // end details, end post
                    $html .= '</div></article>';

                // end loop
                endwhile;

                $html .= $this->getPagination($query->query_vars['paged'], $page_number_max, 5);

            else :

                return \DV_Utils::notice('<strong>No Posts Found</strong>');

            endif;

            return $html;
        }

        /**
         * Echo the archive sort-order HTML.
         *  
         * @since 1.0.0
         * 
         * @access public
         * @return void
         * @see DV_Posts::getSort()
         */
        public function sort()
        {
            echo $this->getSort();
        }

        /**
         * Get the archive sort-order HTML.
         * 
         * Generate a sort-order select box with labels and values.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return string archive HTML in string format.
         * @see DV_Posts::getSortOptions()
         */
        public function getSort()
        {
            $html = '';
            $optHTML = '';
            $sort_var = $this->qVars['sort']['value'];
            $theOptions = $this->getSortOptions();

            foreach ($theOptions as $key => $args) {
                $optHTML .= '<option class="dv-filter-sort-item" value="' . $key . '"';
                if ($key == $sort_var) $optHTML .= ' selected="selected"';
                $optHTML .= '>' . $args['label'] . '</option>';
            }

            $html .= '<select class="dv-filter-sort">' . $optHTML . '</select>';

            return $html;
        }

        /**
         * Returns the options for the sort-order of this archive.
         * 
         * $options is an accociative array. Each key is the option name,
         * and the value is another associative array; label, query arguments, etc.
         * 
         * A key in $options is used as the "value" attribute of a select option in HTML,
         * the contained label is also used as such.
         * 
         * When a sort-order change is requested, the select option "value" attribute is
         * used to identify the correct key in the $options array, then the WP Query arguements
         * are used to amend the query.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @uses filter dv_posts_orderby_options
         * @return array Sort-order option labels, names and accociated arguements for the WP Query.
         */
        public function getSortOptions()
        {
            $options = array(
                'alpha-asc' => [
                    'label' => 'Alphabetically: A - Z',
                    'wp_query_args' => [
                        'orderby' => 'title',
                        'order' => 'ASC',
                    ]
                ],
                'alpha-desc' => [
                    'label' => 'Alphabetically: Z - A',
                    'wp_query_args' => [
                        'orderby' => 'title',
                        'order' => 'DESC',
                    ]
                ],
                'date-desc' => [
                    'label' => 'Newest First',
                    'wp_query_args' => [
                        'orderby' => 'date',
                        'order' => 'DESC',
                    ]
                ],
                'date-asc' => [
                    'label' => 'Oldest first',
                    'wp_query_args' => [
                        'orderby' => 'date',
                        'order' => 'ASC',
                    ]
                ],
            );

            /**
             * Filter sort-order options on DV Posts archives.
             * 
             * Example:
             * 
             *     add_filter('dv_posts_orderby_options', 'setSortOptions');
             *     function setSortOptions($options, $postType)
             *     {
             *         if ($postType == 'tribe_events') {
             *             unset($options['date-asc']);
             *             unset($options['date-desc']);
             *             $newOptions = [
             *                 'start-date-desc' => [
             *                     'label' => 'Start Date - Latest First',
             *                     'wp_query_args' => [
             *                         'meta_key' => '_EventStartDate',
             *                         'orderby' => '_EventStartDate',
             *                         'order' => 'DESC'
             *                     ]
             *                 ],
             *                 'start-date-asc' => [
             *                     'label' => 'Start Date - Earliest First',
             *                     'wp_query_args' => [
             *                         'meta_key' => '_EventStartDate',
             *                         'orderby' => '_EventStartDate',
             *                         'order' => 'ASC'
             *                     ]
             *                 ],
             *             ];
             *             foreach ($newOptions as $key => $val) {
             *                 $options = array($key => $val) + $options;
             *             }
             *         }
             *         return $options;
             *     }
             *
             * @since 1.0.0
             * 
             * @param array $options Accociative array containing default sort-order options.
             * @param string|array $postType The post type(s) of the archive.
             * @param int $id The current post/page.
             */
            $options = apply_filters('dv_posts_orderby_options', $options, $this->wpQArgs['post_type']);

            return $options;
        }

        /**
         * Add additional Sort arguements to the WP Query arguments.
         * 
         * @since 1.0.1
         * 
         * @access private
         * @return void
         */
        private function applySort(): void
        {
            if(!isset($this->wpQArgs['meta_query'])) {
                $this->wpQArgs['meta_query'] = [];
            }
            $this->wpQArgs['meta_query']['relation'] = 'AND';

            // ** Sort Query Variable
            $sortVar = isset($this->qVars['sort']['value']) ? $this->qVars['sort']['value'] : null;
            $theSortOption = null;
            $sortOptions = $this->getSortOptions();

            // select the current order
            if (isset($sortOptions[$sortVar])) {
                $theSortOption = $sortOptions[$sortVar];
            } else {
                $theSortOption = current($sortOptions);
            }

            if ($theSortOption !== null) {

                if (!isset($theSortOption['wp_query_args'])) return;

                $sortArgs = $theSortOption['wp_query_args'];

                if (isset($sortArgs['meta_key'])) {
                    $this->wpQArgs['meta_key'] = $sortArgs['meta_key'];
                }

                if (isset($sortArgs['orderby'])) {
                    $this->wpQArgs['orderby'] = $sortArgs['orderby'];
                }

                if (isset($sortArgs['order'])) {
                    $this->wpQArgs['order'] = $sortArgs['order'];
                }
            }
        }

        /**
         * Get the HTML for the filter of this archive.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return string the HTML for filters.
         * @see DV_Posts::getSearchForm()
         */
        public function searchForm()
        {
            echo $this->getSearchForm();
        }

        /**
         * Get the HTML for the filter of this archive.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return string the HTML for filters.
         */
        public function getSearchForm()
        {
            $html = '<form class="dv-filter-search" method="GET"><input class="dv-filter-search-input" type="search" id="dv-search" name="dv_search" value="' . get_query_var('dv_search') . '" placeholder="Search..."><button type="submit" class="dv-submit">Search</button></form>';
            return $html;
        }

        /**
         * Echo the HTML for the filter of this archive.
         *
         * @since 1.0.0
         * 
         * @access public
         * @return void
         * @see DV_Posts::getTaxonomyFilters()
         */
        public function taxonomyFilters()
        {
            echo $this->getTaxonomyFilters();
        }

        /**
         * Get the HTML for the filter of this archive.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @uses filter dv_posts_tax_filters_terms_args
         * @return string the HTML for filters.
         */
        public function getTaxonomyFilters()
        {
            $html = '';

            if ($this->taxonomies) {

                $html .= '<div class="dv-posts-taxonomy-filters" data-prefix="' . esc_attr($this->prefix) . '">';
                foreach ($this->taxonomies as $taxonomy) :

                    $taxVarKey = $this->prefix . '_' . $taxonomy;
                    $taxVar = get_query_var($taxVarKey) ? get_query_var($taxVarKey) : null;
                    $optArgs = [
                        'taxonomy' => $taxonomy,
                        'hide_empty' => true,
                    ];

                    /**
                     * Filter the taxonomy arguements for the archive.
                     * 
                     * When taxonomies are set for the DV_Posts object, a list of buttons is generated
                     * in HTML for each taxonomy, which are in turn used for filtering posts in the archive.
                     * 
                     * In the loop, each taxonomy is provided to get_terms(), this is where you can amend the arguements.
                     * 
                     * @since 1.0.0
                     * @param array $optArgs The 
                     * @param array|string $postType 
                     * @param string $taxonomy the current taxonomy in the loop
                     * 
                     */
                    $optArgs = apply_filters('dv_posts_tax_filters_terms_args', $optArgs, $this->wpQArgs['post_type'], $taxonomy);
                    $options = get_terms($optArgs);

                    $html .= '<ul class="dv-posts-taxonomy-filter" data-taxonomy="' . esc_attr($taxonomy) . '">';
                    foreach ($options as $opt) :
                        $class = 'ajax_item terms-list_item';
                        if ($opt->slug == $taxVar) {
                            $class .= ' active';
                        }
                        $html .= '<li><button class="' . esc_attr($class) . '" data-slug="' . esc_attr($opt->slug) . '">' . esc_html($opt->name) . '</button></li>';
                    endforeach;
                    $html .= '</ul>';

                endforeach;
                $html .= '</div>';
            }

            return $html;
        }

        /**
         * Add the actions for the DV_Posts ajax callback function.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         */
        public static function addAjaxActions()
        {
            add_action('wp_ajax_dvFilterArchive', [get_called_class(), 'dvFilterArchive']);
            add_action('wp_ajax_nopriv_dvFilterArchive', [get_called_class(), 'dvFilterArchive']);
        }

        /**
         * The Ajax callback function which instantiates the new archive.
         * 
         * New values are filtered and sanitised, a new instance of DV_Posts is created with
         * the arguements from the ajax request.
         * 
         * The name of this function is referred to in the ajax.js file.
         * 
         * @since 1.0.0
         * 
         * @access public
         * @return void
         */
        public function dvFilterArchive()
        {
            $ajxData = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $ajxSort = $ajxData['sort'];
            $ajxWpQArgs = $ajxData['wpQArgs'];
            $ajxTaxonomies = $ajxData['taxonomies'];

            $dvBackupImg = null;
            $dvPrefix = null;

            if (!isset( $ajxData ) || ! Utils::validate_array($ajxData)) {
                echo Utils::notice( '<strong>DV Posts error:</strong> there was an issue with ajax response data.', 'dv-error' );
                exit;
            }

            if (isset($ajxData['prefix']) && Utils::validate_string($ajxData['prefix'], '/^[a-z-_]{1,10}$/')) {
                $dvPrefix = sanitize_text_field($ajxData['prefix']);
            } else {
                echo Utils::notice('<strong>DV Posts error:</strong> ajax["prefix"] is invalid.', 'dv-error');
                exit;
            }

            if (isset($ajxData['taxonomies'])) {
                $dvTaxonomies = $ajxData['taxonomies'];
            }

            if (isset($ajxData['wpQArgs'])) {

                if (isset($ajxWpQArgs['tax_query'])) {

                    $taxQueries = $ajxWpQArgs['tax_query'];
                    unset($taxQueries['relation']);

                    foreach ($taxQueries as $taxQuery) {
                        $taxVar = $dvPrefix . '_' . $taxQuery['taxonomy'];

                        if (taxonomy_exists($taxQuery['taxonomy']) && term_exists($taxQuery['terms'], $taxQuery['taxonomy'])) {
                            set_query_var($taxVar, sanitize_text_field($taxQuery['terms']));
                        } elseif (get_query_var($taxVar)) {
                            set_query_var($taxVar, null);
                        }
                    }
                }

                if (isset($ajxWpQArgs['paged'])) {
                    set_query_var($dvPrefix . '_page', filter_var($ajxWpQArgs['paged'], FILTER_SANITIZE_NUMBER_INT));
                } elseif (get_query_var($dvPrefix . '_page')) {
                    set_query_var($dvPrefix . '_page', null);
                }

                if (isset($ajxSort)) {
                    set_query_var($dvPrefix . '_sort', sanitize_text_field($ajxSort));
                } elseif (get_query_var($dvPrefix . '_sort')) {
                    set_query_var($dvPrefix . '_sort', null);
                }

                if (isset($ajxWpQArgs['s'])) {
                    set_query_var($dvPrefix . '_search', sanitize_text_field($ajxWpQArgs['s']));
                } elseif (get_query_var($dvPrefix . '_search')) {
                    set_query_var($dvPrefix . '_search', null);
                }
            } else {
                echo Utils::notice('<strong>DV Posts error:</strong> ajax["wp_query"] is invalid.', 'dv-error');
                exit;
            }

            $DVPosts = new Posts([
                'wp_query_args' => $ajxWpQArgs,
                'taxonomies' => $ajxTaxonomies,
                'use_qvars' => true,
                'backup_img' => $dvBackupImg
            ]);
            $response = '';
            $response .= $DVPosts->getArchive();

            echo $response;

            exit;
        }

        /**
         * Get the archive pagination HTML.
         * 
         * @since 1.0.0
         * 
         * @global $wp_query
         * @access public
         * @param int|null $paged Optional. The current page of the query.
         * @param int|null $pages Optional. The total amount of pages in the query.
         * @param int $range Optional. The range in which pagination numbers are shown together at either end before the count is broken from the first and last pages.
         * 
         * @return string The pagination HTML.
         */
        public function getPagination($paged = null, $pages = null, int $range = 4)
        {
            /**
             * current page
             */
            $output = '';

            /**
             * $range divided by two
             * integer for finding the items on either side of the current page item
             */
            $showitems = ($range / 2);

            if (null == $paged) {
                $paged = 1;
            }

            // Total number of pages
            if ($pages == null) {
                global $wp_query;
                $pages = wp_count_posts($this->wpQArgs['post_type'])->publish;
                if (!$pages) $pages = 1;
            }

            // If there are more than 1 total pages in the query
            if (1 != $pages) {

                $output .= '
                <nav class="pagination" aria-label="Page navigation">
                <ul>';

                /**
                 * Previous page link
                 * 
                 * The current page must not be the first.
                 */
                if ($paged != 1) {
                    $output .= '<li class="page-item previous"><button class="ajax_item pag_item" data-page="' . ($paged - 1) . '"><i class="fas fa-arrow-circle-left"></i><span class="pag_item_text"> Previous</span></button></li>';
                }

                /**
                 * First page link
                 * 
                 * Anchors the pagination to the first page, if needed.
                 * 
                 * The current page must not be the first page,
                 * and must be at least $showitems ( half the $range ) away from it.
                 */
                if ($paged != 1 && $paged >= (1 + $showitems)) {
                    $output .= '<li class="page-item before first"><button class="ajax_item pag_item" data-page="1">1';
                    if ($paged > (1 + ceil($showitems))) $output .= '...';
                    $output .= '</button></li>';
                }

                /**
                 * Before links
                 * 
                 * Take the current page,
                 * Use the $showitems ( $range / 2 ) to get the immediate previous page links.
                 * 
                 * Current page must be at least two pages away from the last page.
                 * 
                 * To achive the correct order of page links,
                 * reverse concatenate $before to itself, and then concatenate this to $output after the loop.
                 */
                $before = '';
                for ($i = 1; $i < $showitems; $i++) {
                    $the_page_num = $paged - $i;
                    $before = ($the_page_num > 0) ? '<li class="page-item before"><button class="ajax_item pag_item" data-page="' . $the_page_num . '">' . $the_page_num . '</button></li>' . $before : $before;
                }
                $output .= $before;

                // Current page item.
                $output .= '<li class="page-item current"><button class="ajax_item pag_item active">' . $paged . '</button></li>';

                /**
                 * After links
                 * 
                 * Take the current page,
                 * Use the $showitems ( $range / 2 ) to get the immediate previous page links.
                 * 
                 * Current page must be at least two pages away from the last page.
                 */
                for ($i = 1; $i < $showitems; $i++) {
                    $the_page_num = $paged + $i;
                    $output .= ($the_page_num <= $pages) ? '<li class="page-item after"><button class="ajax_item pag_item" data-page="' . $the_page_num . '">' . $the_page_num . '</button></li>' : '';
                }

                /**
                 * Last page link
                 * 
                 * Anchors the pagination to the last page, if needed.
                 * 
                 * The current page must not be the last page,
                 * and must be at least $showitems ( half the $range ) away from it.
                 */
                if ($paged != $pages && $paged <= ($pages - $showitems)) {
                    $output .= '<li class="page-item after last"><button class="ajax_item pag_item" data-page="' . $pages . '">';
                    if ($paged < ($pages - ceil($showitems))) $output .= '...';
                    $output .= $pages . '</a></li>';
                }

                /**
                 * Next page link
                 * 
                 * The current page must not be the last.
                 */
                if ($paged != $pages) {
                    $output .= '<li class="page-item next"><button class="ajax_item pag_item" data-page="' . ($paged + 1) . '"><span class="pag_item_text">Next </span><i class="fas fa-arrow-circle-right"></i></button></li>';
                }

                $output .= '</ul></nav>';
            }

            return $output;
        }
    }
endif;
