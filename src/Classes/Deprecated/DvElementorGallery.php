<?php

/**
 * BJO102Press Classes: DV_Elementor_Gallery.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

use DVWP\Classes\ElementorGallery;

if (! defined('ABSPATH')) exit;

if (! class_exists('DV_Elementor_Gallery')) :
    /**
     * HTML generator class for media gallery which utilizes Elementor functionality.
     *
     * @since 1.0.0
     * @deprecated
     */
    class DV_Elementor_Gallery extends ElementorGallery {}
endif;