<?php

/**
 * BJO102Press Classes: DV_Utils.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

use DVWP\Classes\Utils;

if (!defined('ABSPATH')) exit;

if (!class_exists('DV_Utils')) :
    /**
     * The BJO102Press helper class.
     * 
     * Helper functions for other plugins and modules to use.
     * 
     * - Getting primary terms.
     * - Data validation.
     * - Plugin activation checks.
     * 
     * @since 1.0.0
     * @deprecated
     */
    class DV_Utils extends Utils {}
endif;