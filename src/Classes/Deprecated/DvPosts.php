<?php

/**
 * BJO102Press Classes: DV_Posts.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

use DVWP\Classes\Posts;

if (!defined('ABSPATH')) exit;

if (!class_exists('DV_Posts')) :
    /**
     * A class for producing post archives.
     * 
     * One instance of this class represents an archive. There are 
     * methods for getting html templates for posts, taxonomy filters, sort-order
     * and search boxes.
     * 
     * 
     * @since 1.0.0
     * @deprecated
     */
    class DV_Posts extends Posts
    {
        public function __construct($args)
        {
            parent::__construct($args);
        }
    }
endif;