<?php

/**
 * BJO102Press Classes: DVWP.
 * 
 * @since 1.0.0
 * 
 * @package BJO102Press\Classes
 */

use DVWP\Classes\Main;

if (!defined('ABSPATH')) exit;

if (!class_exists('DVWP')) :
    /**
     * The BJO102Press main class.
     * 
     * Pulls all the required files and sets adds essential actions and filters.
     * The tasks of this class include the following:
     * 
     * - Ajax hooks for DV_Posts. 
     * - Custom query variables.
     * - The Site Admin user role.
     * - Adminbar customisations.
     * - Base styles and scripts.
     * - Admin footer text.
     *
     * @since 1.0.0
     * @deprecated
     */
    class DVWP extends Main
    {
        public function __construct()
        {
            parent::__construct();
        }
    }
endif;